/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.server;

import com.project.chat.entities.Client;
import com.project.chat.services.Model;
import com.project.chat.services.Setting;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author DongHo
 */
public class MyServer {

    public static void start() {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        // init
        Map<Integer, Client> clientsMap = new HashMap<>();
        Map<Integer, ArrayList<Client>> chatRoomMap = new HashMap<>();
        Model.initChatRoomMap(chatRoomMap);

        try {
            System.out.println("Bắt đầu khởi tạo server....");
            ServerSocket ss = new ServerSocket(Setting.SERVER_PORT);
            System.out.println("Server sẵn sàng!");
            int count = 0; // đếm kết nối
            while (true) {
                Socket socket = ss.accept();
                ++count;
                System.out.println("[" + count + "]. Kết nối thành công với [" + socket.getInetAddress() + "]");
                executorService.execute(new ServerThread(socket, clientsMap, chatRoomMap)::run);
            }
        } catch (IOException ex) {
            System.err.println("Lỗi (ở start@MyServer): " + ex);
        }

        executorService.shutdown();
    }

    public static void main(String[] args) {
        MyServer.start();
    }
}
