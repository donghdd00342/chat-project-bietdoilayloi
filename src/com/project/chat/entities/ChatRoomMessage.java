/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;

/**
 *
 * @author DongPhuc
 */
public class ChatRoomMessage implements Serializable {
    private User user;
    private String Msg;
    private Room room;

    public ChatRoomMessage(User user, String Msg, Room room) {
        this.user = user;
        this.Msg = Msg;
        this.room = room;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String Msg) {
        this.Msg = Msg;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
