/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;

/**
 *
 * @author DongHo
 */
public class PrivateMessage implements Serializable {
     int fromId;
     String fromFullName;
     int toId;
     String toFullName;
     String privateMessage;

     public PrivateMessage(int fromId, String fromFullName, int toId, String toFullName, String privateMessage) {
	  this.fromId = fromId;
	  this.fromFullName = fromFullName;
	  this.toId = toId;
	  this.toFullName = toFullName;
	  this.privateMessage = privateMessage;
     }

     public int getFromId() {
	  return fromId;
     }

     public void setFromId(int fromId) {
	  this.fromId = fromId;
     }

     public String getFromFullName() {
	  return fromFullName;
     }

     public void setFromFullName(String fromFullName) {
	  this.fromFullName = fromFullName;
     }

     public int getToId() {
	  return toId;
     }

     public void setToId(int toId) {
	  this.toId = toId;
     }

     public String getToFullName() {
	  return toFullName;
     }

     public void setToFullName(String toFullName) {
	  this.toFullName = toFullName;
     }

     public String getPrivateMessage() {
	  return privateMessage;
     }

     public void setPrivateMessage(String privateMessage) {
	  this.privateMessage = privateMessage;
     }
}
