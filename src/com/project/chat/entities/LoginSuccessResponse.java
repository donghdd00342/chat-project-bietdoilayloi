/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author DongHo
 */
public class LoginSuccessResponse implements Serializable {

     private HashMap<Integer, String> listFriend;
     private HashMap<Integer, OfflineMessages> ListOfflineMsg;
     private ArrayList<SendFriendRequest> sendFriendRequestList;
     private HashMap<Integer, String> blockList;
     private User user;

     public HashMap<Integer, String> getListFriend() {
	  return listFriend;
     }

     public void setListFriend(HashMap<Integer, String> listFriend) {
	  this.listFriend = listFriend;
     }

     public User getUser() {
	  return user;
     }

     public void setUser(User user) {
	  this.user = user;
     }

     public ArrayList<SendFriendRequest> getSendFriendRequestList() {
	  return sendFriendRequestList;
     }

     public void setSendFriendRequestList(ArrayList<SendFriendRequest> sendFriendRequestList) {
	  this.sendFriendRequestList = sendFriendRequestList;
     }

     public HashMap<Integer, String> getBlockList() {
	  return blockList;
     }

     public void setBlockList(HashMap<Integer, String> blockList) {
	  this.blockList = blockList;
     }

    public HashMap<Integer, OfflineMessages> getListOfflineMsg() {
        return ListOfflineMsg;
    }

    public void setListOfflineMsg(HashMap<Integer, OfflineMessages> ListOfflineMsg) {
        this.ListOfflineMsg = ListOfflineMsg;
    }
}
