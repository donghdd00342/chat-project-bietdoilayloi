-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2017 at 03:32 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--DROP TABLE `chat_rooms`, `logs`, `relationships`, `users`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `messenger_app`
--

-- --------------------------------------------------------

-- DROP TABLE `chat_rooms`, `logs`, `relationships`, `users`;

CREATE TABLE `chat_rooms` (
  `id` int(11) NOT NULL,
  `own` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chat_rooms`
--

-- INSERT INTO `chat_rooms` (`id`, `own`, `name`, `description`, `status`) VALUES
-- (1, 1, 'Tim ban gai', 'Boys tim Girl', 1),
-- (2, 1, 'Tim ban trai', 'Girls tim Boys', 1);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `relationship_id` int(11) NOT NULL,
  `content` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` int(11) NOT NULL,
  `create_at` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `logs`
--

-- INSERT INTO `logs` (`id`, `relationship_id`, `content`, `status`, `create_at`) VALUES
-- (1, 2, 'Xin chào  Xuân Hùng! kết bạn nhé ^.^', -1, 1498480967792),

-- --------------------------------------------------------

--
-- Table structure for table `relationships`
--

CREATE TABLE `relationships` (
  `id` int(11) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relationships`
--

-- INSERT INTO `relationships` (`id`, `sub_id`, `friend_id`, `status`) VALUES
-- (3, 18, 19, -2),

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `full_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `tocken` varchar(50) DEFAULT NULL,
  `access_code` varchar(50) DEFAULT NULL,
  `status` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

-- INSERT INTO `users` (`id`, `email`, `password`, `full_name`, `tocken`, `access_code`, `status`) VALUES
-- (1, 'dong@gmail.com', 'c8LH5zRpWywY2UwbxbpzWw==', 'Hồ Đông', '', '', 1),

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat_rooms`
--
ALTER TABLE `chat_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `relationship_id` (`relationship_id`);

--
-- Indexes for table `relationships`
--
ALTER TABLE `relationships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat_rooms`
--
ALTER TABLE `chat_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `relationships`
--
ALTER TABLE `relationships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- Constraints for dumped tables
--

--
-- Constraints for tables
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_fk` FOREIGN KEY (`relationship_id`) REFERENCES `relationships` (`id`);
  
ALTER TABLE `relationships`
  ADD CONSTRAINT `logs_sub_id_fk` FOREIGN KEY (`sub_id`) REFERENCES `users` (`id`);
ALTER TABLE `relationships`
  ADD CONSTRAINT `logs_friend_id_fk` FOREIGN KEY (`friend_id`) REFERENCES `users` (`id`);
  
ALTER TABLE `chat_rooms`
  ADD CONSTRAINT `logs_chat_rooms_fk` FOREIGN KEY (`own`) REFERENCES `users` (`id`);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
