/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.services.model;

import com.project.chat.services.Setting;

/**
 *
 * @author DongHo
 */
public class ChatRoom implements Table  {
     private int id;
     private int own;
     private String name;
     private String description;
     private int status;

    public ChatRoom(int id, int own, String name, String description, int status) {
        this.id = id;
        this.own = own;
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public ChatRoom(int own, String name, String description, int status) {
        this.own = own;
        this.name = name;
        this.description = description;
        this.status = status;
    }
    
     public ChatRoom(int id) {
	  this.id = id;
     }

     public ChatRoom() {
     }

     public int getId() {
	  return id;
     }

     public void setId(int id) {
	  this.id = id;
     }

     public String getName() {
	  return name;
     }

     public void setName(String name) {
	  this.name = name;
     }

     public String getDescription() {
	  return description;
     }

     public void setDescription(String description) {
	  this.description = description;
     }

     public int getStatus() {
	  return status;
     }

     public void setStatus(int status) {
	  this.status = status;
     }

    public int getOwn() {
        return own;
    }

    public void setOwn(int own) {
        this.own = own;
    }

     @Override
     public String getTable() {
	  return Setting.CHAT_ROOMS_TABLE_NAME;
     }
}
