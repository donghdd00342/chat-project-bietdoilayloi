/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.services.model;

import com.project.chat.services.Setting;

/**
 *
 * @author DongHo
 */
public class Users implements Table  {
     private int id;
     private String email;
     private String password;
     private String full_name;
     private String tocken;
     private String access_code;
     private int status;

     public Users(String email, String password, String full_name, String tocken, String access_code, int status) {
	  this.email = email;
	  this.password = password;
	  this.full_name = full_name;
	  this.tocken = tocken;
	  this.access_code = access_code;
	  this.status = status;
     }

     public Users(int id, String email, String password, String full_name, String tocken, String access_code, int status) {
	  this.id = id;
	  this.email = email;
	  this.password = password;
	  this.full_name = full_name;
	  this.tocken = tocken;
	  this.access_code = access_code;
	  this.status = status;
     }

     public Users(int id) {
	  this.id = id;
     }

     public Users() {
     }

     public int getId() {
	  return id;
     }

     public void setId(int id) {
	  this.id = id;
     }

     public String getEmail() {
	  return email;
     }

     public void setEmail(String email) {
	  this.email = email;
     }

     public String getPassword() {
	  return password;
     }

     public void setPassword(String password) {
	  this.password = password;
     }

     public String getFull_name() {
	  return full_name;
     }

     public void setFull_name(String full_name) {
	  this.full_name = full_name;
     }

     public String getTocken() {
	  return tocken;
     }

     public void setTocken(String tocken) {
	  this.tocken = tocken;
     }

     public String getAccess_code() {
	  return access_code;
     }

     public void setAccess_code(String access_code) {
	  this.access_code = access_code;
     }

     public int getStatus() {
	  return status;
     }

     public void setStatus(int status) {
	  this.status = status;
     }

     @Override
     public String getTable() {
	  return Setting.USERS_TABLE_NAME;
     }
     
}
