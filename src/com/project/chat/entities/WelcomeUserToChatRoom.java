/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author DongPhuc
 */
public class WelcomeUserToChatRoom implements Serializable {
    private ChatRoomMessage chatRoomMessage;
    private ArrayList<User> arrayListClient;

    public WelcomeUserToChatRoom(ChatRoomMessage chatRoomMessage, ArrayList<User> arrayListClient) {
        this.chatRoomMessage = chatRoomMessage;
        this.arrayListClient = arrayListClient;
    }

    public ChatRoomMessage getChatRoomMessage() {
        return chatRoomMessage;
    }

    public void setChatRoomMessage(ChatRoomMessage chatRoomMessage) {
        this.chatRoomMessage = chatRoomMessage;
    }

    public ArrayList<User> getArrayListClient() {
        return arrayListClient;
    }

    public void setArrayListClient(ArrayList<User> arrayListClient) {
        this.arrayListClient = arrayListClient;
    }
}
