/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

/**
 *
 * @author DongHo
 */
public class Actions {

     private Actions() {
     }

     public static final int LOGIN_SUCCESS = 1;
     public static final int REGISTER = 2;
     public static final int REGISTRATION_CONFIRMATION = 3;
     public static final int RESEND_CONFIRMATION_CODE = 4;
     public static final int FORGET_PASSWORD = 5;
     public static final int SEARCH_FRIENDS = 6;
     public static final int SEND_FRIEND_REQUEST = 7;
     public static final int ASK_FRIEND_RESPONSE = 17;
     public static final int EXISTING_RELATIONSHIP = 16;
     public static final int ASK_FRIEND = 8;
     public static final int CONFIRM_FRIEND = 9;
     public static final int USER_DELETE_FRIEND = 10;
     public static final int CLIENT_LOGOUT = 11;
     public static final int JOIN_TO_CHAT_ROOM = 12;
     public static final int GET_OUT_FROM_CHAT_ROOM = 13;
     public static final int SEND_MESSAGE_TO_CHAT_ROOM = 14;
     public static final int SEND_PRIVATE_MESSAGE = 15;
     public static final int REFUSE_FRIEND_REQUEST = 18;
     public static final int USER_SET_UNBLOCK = 19;
     public static final int GET_HISTORY_PRIVATE_CHAT = 20;
     public static final int GET_CHAT_ROOM_LIST = 21;
     public static final int CREATE_CHAT_ROOM = 22;
     public static final int DELETE_CHAT_ROOM = 23;
     public static final int KICK_MEMBER_FROM_CHAT_ROOM = 24;
     public static final int USER_LEAVE_FROM_CHAT_ROOM = 25;
     public static final int WELCOME_USER_TO_CHAT_ROOM = 26;
     public static final int READ_OFFLINE_MSG = 27;

     public static class Update {

	  private Update() {
	  }
	  public static final int FRIENDS_LIST_BEFORE_AGREE = 51;
	  public static final int UNREAD_MESSAGE = 52;
	  public static final int WAITING_FRIEND = 53;
	  public static final int FRIEND_REQUEST_FLOW_ADD_FRIEND = 53;
	  public static final int REQUEST_FRIEND_LIST_BEFORE_REFUSE = 54;
	  public static final int USER_UPDATE_INFORMATIONS = 55;
	  public static final int USER_UPDATE_INFORMATIONS_RESPONSE = 56;
	  public static final int USER_UPDATE_PASSWORD = 57;
     }

}
