/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author DongHo
 */
public class GetHistoryPrivateChat implements Serializable {

     private int userId;
     private String userName;
     private int friendId;
     private String friendName;
     private int limitCount;
     private int viewOption;
     private HashMap<Long, String> listMsg;

     public GetHistoryPrivateChat(int userId, String userName, int friendId, String friendName, int limitCount, int viewOption) {
	  this.userId = userId;
	  this.userName = userName;
	  this.friendId = friendId;
	  this.friendName = friendName;
	  this.limitCount = limitCount;
	  this.viewOption = viewOption;
     }

     public String getUserName() {
	  return userName;
     }

     public void setUserName(String userName) {
	  this.userName = userName;
     }

     public String getFriendName() {
	  return friendName;
     }

     public void setFriendName(String friendName) {
	  this.friendName = friendName;
     }

     public int getUserId() {
	  return userId;
     }

     public void setUserId(int userId) {
	  this.userId = userId;
     }

     public int getFriendId() {
	  return friendId;
     }

     public void setFriendId(int friendId) {
	  this.friendId = friendId;
     }

     public int getLimitCount() {
	  return limitCount;
     }

     public void setLimitCount(int limitCount) {
	  this.limitCount = limitCount;
     }

     public int getViewOption() {
	  return viewOption;
     }

     public void setViewOption(int viewOption) {
	  this.viewOption = viewOption;
     }

     public HashMap<Long, String> getListMsg() {
	  return listMsg;
     }

     public void setListMsg(HashMap<Long, String> listMsg) {
	  this.listMsg = listMsg;
     }
}
