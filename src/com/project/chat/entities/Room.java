/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;

/**
 *
 * @author DongPhuc
 */
public class Room implements Serializable {

    private int roomId;
    private String roomName;
    private String roomDescripton;
    private int ownId;
    private String ownName;

    public Room(int roomId, String roomName, String roomDescripton, int ownId, String ownName) {
        this.roomId = roomId;
        this.roomName = roomName;
        this.roomDescripton = roomDescripton;
        this.ownId = ownId;
        this.ownName = ownName;
    }

    public Room(String roomName, String roomDescripton, int ownId) {
        this.roomName = roomName;
        this.roomDescripton = roomDescripton;
        this.ownId = ownId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomDescripton() {
        return roomDescripton;
    }

    public void setRoomDescripton(String roomDescripton) {
        this.roomDescripton = roomDescripton;
    }

    public int getOwnId() {
        return ownId;
    }

    public void setOwnId(int ownId) {
        this.ownId = ownId;
    }

    public String getOwnName() {
        return ownName;
    }

    public void setOwnName(String ownName) {
        this.ownName = ownName;
    }

    @Override
    public String toString() {
        return "Room{" + "roomId=" + roomId + ", roomName=" + roomName + ", roomDescripton=" + roomDescripton + ", ownId=" + ownId + ", ownName=" + ownName + '}';
    }

}
