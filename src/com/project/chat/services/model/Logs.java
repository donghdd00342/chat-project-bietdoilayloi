/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.services.model;

import com.project.chat.services.Setting;

/**
 *
 * @author DongHo
 */
public class Logs implements Table {

     private int id;
     private int relationship_id;
     private String content;
     private int status;
     private long create_at;

     public Logs(int relationship_id, String content, int status, long create_at) {
	  this.relationship_id = relationship_id;
	  this.content = content;
	  this.status = status;
	  this.create_at = create_at;
     }

     public Logs(int id, int relationship_id, String content, int status, long create_at) {
	  this.id = id;
	  this.relationship_id = relationship_id;
	  this.content = content;
	  this.status = status;
	  this.create_at = create_at;
     }

     public Logs(int id) {
	  this.id = id;
     }

     public Logs() {
     }

     public int getId() {
	  return id;
     }

     public void setId(int id) {
	  this.id = id;
     }

     public int getRelationship_id() {
	  return relationship_id;
     }

     public void setRelationship_id(int relationship_id) {
	  this.relationship_id = relationship_id;
     }

     public String getContent() {
	  return content;
     }

     public void setContent(String content) {
	  this.content = content;
     }

     public int getStatus() {
	  return status;
     }

     public void setStatus(int status) {
	  this.status = status;
     }

     public long getCreate_at() {
	  return create_at;
     }

     public void setCreate_at(long create_at) {
	  this.create_at = create_at;
     }

     @Override
     public String getTable() {
	  return Setting.LOGS_TABLE_NAME;
     }

}
