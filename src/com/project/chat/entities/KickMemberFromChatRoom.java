/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;

/**
 *
 * @author DongPhuc
 */
public class KickMemberFromChatRoom implements Serializable {
    private User fromUser;
    private int targetId;
    private String targerName;
    private Room fromRoom;

    public KickMemberFromChatRoom(User fromUser, int targetId, String targerName, Room fromRoom) {
        this.fromUser = fromUser;
        this.targetId = targetId;
        this.targerName = targerName;
        this.fromRoom = fromRoom;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public String getTargerName() {
        return targerName;
    }

    public void setTargerName(String targerName) {
        this.targerName = targerName;
    }

    public Room getFromRoom() {
        return fromRoom;
    }

    public void setFromRoom(Room fromRoom) {
        this.fromRoom = fromRoom;
    }
}
