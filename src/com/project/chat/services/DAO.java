/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ngocplh
 */
public class DAO {

    private static final String HOST = "localhost:";
    private static final String PORT = "3306/";
    private static final String DB_NAME = "messenger_app?useUnicode=true&characterEncoding=utf-8";
    private static final String USER_NAME = "root";
    private static final String PASSWORD = "";//"1@34567890";

     public static Connection dbConnect() throws SQLException {
	  String url = "jdbc:mysql://" + HOST + PORT + DB_NAME;
	  Connection conn = DriverManager.getConnection(url, USER_NAME, PASSWORD);
	  return conn;
     }
}
