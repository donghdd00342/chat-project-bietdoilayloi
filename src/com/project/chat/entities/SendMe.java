/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;

/**
 * @author DongHo
 */
public class SendMe implements Serializable {

     private int typeOfMessage;
     private Object MessageObject;

     public SendMe() {
     }

     public SendMe(int typeOfMessage, Object MessageObject) {
	  this.typeOfMessage = typeOfMessage;
	  this.MessageObject = MessageObject;
     }

     public int getTypeOfMessage() {
	  return typeOfMessage;
     }

     public void setTypeOfMessage(int typeOfMessage) {
	  this.typeOfMessage = typeOfMessage;
     }

     public Object getMessageObject() {
	  return MessageObject;
     }

     public void setMessageObject(Object MessageObject) {
	  this.MessageObject = MessageObject;
     }

}
