/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;

/**
 *
 * @author DongHo
 */
public class User implements Serializable {

     private int id;
     private String mail;
     private String password;
     private String fullname;
     private String token;
     private String accessCode;
     private int status;

     public User() {
     }

     public User(int id, String mail, String password, String fullname, String token, String accessCode, int status) {
	  this.id = id;
	  this.mail = mail;
	  this.password = password;
	  this.fullname = fullname;
	  this.token = token;
	  this.accessCode = accessCode;
	  this.status = status;
     }

     public int getId() {
	  return id;
     }

     public void setId(int id) {
	  this.id = id;
     }

     public String getMail() {
	  return mail;
     }

     public void setMail(String mail) {
	  this.mail = mail;
     }

     public String getPassword() {
	  return password;
     }

     public void setPassword(String password) {
	  this.password = password;
     }

     public String getFullname() {
	  return fullname;
     }

     public void setFullname(String fullname) {
	  this.fullname = fullname;
     }

     public String getAccessCode() {
	  return accessCode;
     }

     public void setAccessCode(String accessCode) {
	  this.accessCode = accessCode;
     }

     public int getStatus() {
	  return status;
     }

     public void setStatus(int status) {
	  this.status = status;
     }

     public String getToken() {
	  return token;
     }

     public void setToken(String token) {
	  this.token = token;
     }
}
