/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.services;

/**
 *
 * @author DongHo
 */
public class Setting {

     private Setting() {
     }
     // server
     public static final String SERVER_IP = "localhost";
     public static final int SERVER_PORT = 9087;
     public static final int SIZE_OF_PACKGE = 1024;
     // database
     public static String CHAT_ROOMS_TABLE_NAME = "chat_rooms";
     public static String LOGS_TABLE_NAME = "logs";
     public static String RELATIONSHIPS_TABLE_NAME = "relationships";
     public static String USERS_TABLE_NAME = "users";
     // email
     public static String SYS_EMAIL = "hodong124@gmail.com";
     public static String SYS_PASSWORD = "1@34567890";
     // security
     public static String SECURITY_INIT_VECTOR = "qWeRTYuIOPLKjHGf"; // 16bit
     public static String SECURITY_KEY = "123456789)!@#$%^"; // 16bit

}
