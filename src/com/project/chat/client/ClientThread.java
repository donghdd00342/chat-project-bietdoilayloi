/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.client;

import com.project.chat.client.ui.AskFriendUI;
import com.project.chat.client.ui.ChosingChatRoomUI;
import com.project.chat.client.ui.MainUserUI;
import com.project.chat.client.ui.PrivateChatUI;
import com.project.chat.client.ui.PublicChatRoomUI;
import com.project.chat.entities.LoginSuccessResponse;
import com.project.chat.entities.SendMe;
import com.project.chat.entities.Actions;
import com.project.chat.entities.ChatRoomMessage;
import com.project.chat.entities.ChatRoomsList;
import com.project.chat.entities.Client;
import com.project.chat.entities.ExistingRelationship;
import com.project.chat.entities.GetHistoryPrivateChat;
import com.project.chat.entities.JoinToChatRoom;
import com.project.chat.entities.KickMemberFromChatRoom;
import com.project.chat.entities.LetReload;
import com.project.chat.entities.PrivateMessage;
import com.project.chat.entities.Room;
import com.project.chat.entities.SendFriendRequest;
import com.project.chat.entities.User;
import com.project.chat.entities.WelcomeUserToChatRoom;
import com.project.chat.services.Untilities;
import java.awt.Frame;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author DongHo
 */
public class ClientThread extends Thread {

    Socket socket;
    User user;
    MainUserUI mainUserUI;
    ChosingChatRoomUI chosingChatRoomUI;
    HashMap<Integer, String> listUserResult;

    HashMap<Integer, PrivateChatUI> privateChatList;
    HashMap<Integer, PublicChatRoomUI> publicChatRoomList;

    ArrayList<Integer> hasSentRequestList;

    public ClientThread(Socket socket) {
        this.socket = socket;
        // khởi tạo danh sách HashMap<Integer, PrivateChatUI> privateChatList
        this.privateChatList = new HashMap<>();
        // khởi tạo danh sách HashMap<Integer, PublicChatRoomUI> publicChatRoomList;
        this.publicChatRoomList = new HashMap<>();
        // khởi tạo danh sách đã gửi yêu cầu kết bạn
        this.hasSentRequestList = new ArrayList<>();
    }

    @Override
    public void run() {
        try {
//	       byte[] box = new byte[Setting.SIZE_OF_PACKGE];
//	       DatagramPacket packet = new DatagramPacket(box, box.length); // recvBuf
//	       DatagramSocket dgs = new DatagramSocket(socket.getLocalPort());
//	       ByteArrayInputStream byteStream;
            ObjectInputStream is;
            SendMe o;
            while (true) {
                System.out.println("Client chờ response...............................");
                is = new ObjectInputStream(this.socket.getInputStream());
                o = (SendMe) is.readObject();
                switch (o.getTypeOfMessage()) {
                    case Actions.LOGIN_SUCCESS:
                        LoginSuccessResponse loginSuccessResponse = (LoginSuccessResponse) o.getMessageObject();
                        // set this.user
                        this.user = loginSuccessResponse.getUser();
                        if (this.user.getFullname() != null) {
                            // mở cửa sổ chính
                            this.mainUserUI = new MainUserUI(
                                    this.socket,
                                    loginSuccessResponse,
                                    this.privateChatList,
                                    this.publicChatRoomList,
                                    this.chosingChatRoomUI,
                                    this.hasSentRequestList
                            );
                            this.mainUserUI.setVisible(true);
                        } else {
                            Untilities.alert("Lỗi gì đó từ server! vui lòng thử lại sau...");
                        }
                        break;
                    case Actions.SEARCH_FRIENDS:
                        this.listUserResult = (HashMap<Integer, String>) o.getMessageObject();
                        this.mainUserUI.fillFriendsResultList(listUserResult);
                        break;
                    case Actions.EXISTING_RELATIONSHIP:
                        ExistingRelationship existingRelationship = (ExistingRelationship) o.getMessageObject();
                        Untilities.alert(this.mainUserUI, existingRelationship.getMessage());
                        break;
                    case Actions.SEND_FRIEND_REQUEST:
                        SendFriendRequest sendFriendRequest = (SendFriendRequest) o.getMessageObject();
                        // hiện cửa sổ hỏi đồng ý kết bạn
                        AskFriendUI askFriend = new AskFriendUI(sendFriendRequest, socket);
                        askFriend.setVisible(true);
                        break;
                    case Actions.Update.FRIENDS_LIST_BEFORE_AGREE:
                        LetReload letReload = (LetReload) o.getMessageObject();
                        // reload friendList
                        this.mainUserUI.fillFriendList(letReload.getListFriend());
                        // reload friendRequest
                        this.mainUserUI.fillFriendRequestList(letReload.getSendFriendRequestList());
                        // hasSentRequestList.remove(tempId)
                        this.mainUserUI.removeHasSentRequestList(letReload.getTempId());
                        // reloadFriendList
                        this.mainUserUI.reloadFriendList();
                        break;
                    case Actions.REFUSE_FRIEND_REQUEST:
                        LetReload letReload2 = (LetReload) o.getMessageObject();
                        // thông báo từ chối kết bạn
                        this.mainUserUI.showAlert(letReload2.getTempMessage());
                        // hasSentRequestList.remove(tempId)
                        this.mainUserUI.removeHasSentRequestList(letReload2.getTempId());
                        break;
                    case Actions.Update.FRIEND_REQUEST_FLOW_ADD_FRIEND:
                        LetReload letReload3 = (LetReload) o.getMessageObject();
                        // cập nhật danh sách yêu cầu kết bạn
                        this.mainUserUI.fillFriendRequestList(letReload3.getSendFriendRequestList());
                        break;
                    case Actions.Update.REQUEST_FRIEND_LIST_BEFORE_REFUSE:
                        LetReload letReload4 = (LetReload) o.getMessageObject();
                        // reload friendRequest
                        this.mainUserUI.fillFriendRequestList(letReload4.getSendFriendRequestList());
                        // reload blockList
                        this.mainUserUI.fillBlockList(letReload4.getBlockList());
                        break;
                    case Actions.USER_DELETE_FRIEND:
                        LetReload letReload5 = (LetReload) o.getMessageObject();
                        // thông báo
                        this.mainUserUI.showAlert(letReload5.getTempMessage());
                        // tải lại danh sách bạn
                        this.mainUserUI.fillFriendList(letReload5.getListFriend());
                        break;
                    case Actions.USER_SET_UNBLOCK:
                        LetReload letReload6 = (LetReload) o.getMessageObject();
                        // thông báo
                        this.mainUserUI.showAlert(letReload6.getTempMessage());
                        // tải lại danh sách block
                        this.mainUserUI.fillBlockList(letReload6.getBlockList());
                        break;
                    case Actions.Update.USER_UPDATE_INFORMATIONS_RESPONSE:
                        this.mainUserUI.userInformations.showAlert((String) o.getMessageObject());
                        break;
                    case Actions.SEND_PRIVATE_MESSAGE:
                        PrivateMessage privateMessage = (PrivateMessage) o.getMessageObject();
                        // kiểm tra xem id thằng gửi có trong danh sách đang chát ko?
                        if (privateChatList.containsKey(privateMessage.getFromId())) {
                            // có thì kiểm tra tiếp xem nó có đang ẩn ko? có thì hiện của sổ đó lên... focus
                            if (privateChatList.get(privateMessage.getFromId()).isVisible()) {
                                // focus...
                                privateChatList.get(privateMessage.getFromId()).setState(Frame.NORMAL);
                            } else {
                                // show
                                privateChatList.get(privateMessage.getFromId()).setVisible(true); // có phiền khi đang chat...?
                            }
                            // append tin nhắn
                            privateChatList.get(privateMessage.getFromId()).txtAreaCurrentChat.append(
                                    " [" + privateMessage.getFromFullName() + "] " + privateMessage.getPrivateMessage() + "\n"
                            );
                        } else {
                            // tạo cửa sổ chat riêng với id thằng gửi (from)
                            PrivateChatUI temp_ = new PrivateChatUI(privateMessage.getFromId(), privateMessage.getFromFullName(), this.socket, this.user);
                            privateChatList.put(privateMessage.getFromId(), temp_);
                            temp_.setVisible(true);
                            // append tin nhắn
                            temp_.txtAreaCurrentChat.append(
                                    " [" + privateMessage.getFromFullName() + "] " + privateMessage.getPrivateMessage() + "\n"
                            );
                        }
                        break;
                    case Actions.GET_HISTORY_PRIVATE_CHAT:
                        GetHistoryPrivateChat getHistoryPrivateChat = (GetHistoryPrivateChat) o.getMessageObject();
                        privateChatList.get(getHistoryPrivateChat.getFriendId()).fillHistoryChat(getHistoryPrivateChat);
                        break;
                    case Actions.GET_CHAT_ROOM_LIST:
                        ChosingChatRoomUI.setChatRoomsList((ChatRoomsList) o.getMessageObject());
                        break;
                    case Actions.CREATE_CHAT_ROOM:
                        Room room = (Room) o.getMessageObject();
                        System.out.println("room = " + room);
                        if (room != null) {
                            Untilities.alert("Tạo thành công phòng [" + room.getRoomName() + "]!");
                            Untilities.sendTo(new SendMe(Actions.GET_CHAT_ROOM_LIST, this.user), socket);
                        } else {
                            Untilities.alert("Tên phòng đã tồn tại, vui lòng lấy tên khác!");
                        }
                        break;
                    case Actions.DELETE_CHAT_ROOM:
                        Room roomDelete = (Room) o.getMessageObject();
                        if (roomDelete != null) {
                            Untilities.sendTo(new SendMe(Actions.GET_CHAT_ROOM_LIST, this.user), socket);
                            Untilities.alert("Xóa thành công phòng [" + roomDelete.getRoomName() + "]!");
                        } else {
                            Untilities.alert("Vẫn có người trong phòng nên không thể xóa!");
                        }
                        break;
                    case Actions.JOIN_TO_CHAT_ROOM:
                        JoinToChatRoom joinToChatRoom = (JoinToChatRoom) o.getMessageObject();
                        if (joinToChatRoom.getMemberList() != null) {
                            // kiểm tra trong hashMapPublicChatRoom có thằng joinToChatRoom.getRoomId() ko?
                            if (this.publicChatRoomList.containsKey(Integer.valueOf(joinToChatRoom.getRoom().getRoomId()))) {
                                // có thì visibale và focus nó lên
                                this.publicChatRoomList.get(Integer.valueOf(joinToChatRoom.getRoom().getRoomId())).setVisible(true);
                                this.publicChatRoomList.get(Integer.valueOf(joinToChatRoom.getRoom().getRoomId())).setState(Frame.NORMAL);
                            } else {
                                // ko thì tạo mới -> thêm nó vào danh sách -> hiện nó lên
                                this.publicChatRoomList.put(
                                        Integer.valueOf(joinToChatRoom.getRoom().getRoomId()),
                                        new PublicChatRoomUI(joinToChatRoom, socket, this.hasSentRequestList, this.publicChatRoomList)
                                );
                                this.publicChatRoomList.get(Integer.valueOf(joinToChatRoom.getRoom().getRoomId())).setVisible(true);
                            }
                        } else {
                            Untilities.alert("Có gì đó ko đúng, ko vào đc phòng...");
                        }
                        break;
                    case Actions.SEND_MESSAGE_TO_CHAT_ROOM:
                        ChatRoomMessage chatRoomMessage = (ChatRoomMessage) o.getMessageObject();
                        this.publicChatRoomList
                                .get(Integer.valueOf(chatRoomMessage.getRoom().getRoomId())).txtAreaChatRoom.append(chatRoomMessage.getMsg());
                        break;
                    case Actions.WELCOME_USER_TO_CHAT_ROOM:
                        WelcomeUserToChatRoom welcomeUserToChatRoom = (WelcomeUserToChatRoom) o.getMessageObject();
                        PublicChatRoomUI temp_PublicChatRoomUI = this.publicChatRoomList
                                .get(Integer.valueOf(welcomeUserToChatRoom.getChatRoomMessage().getRoom().getRoomId()));
                        // load lại danh sách
                        temp_PublicChatRoomUI.userMemberList = welcomeUserToChatRoom.getArrayListClient();
                        temp_PublicChatRoomUI.loadMemberList();
                        // gửi tin nhắn trong phòng
                        temp_PublicChatRoomUI.txtAreaChatRoom.append(welcomeUserToChatRoom.getChatRoomMessage().getMsg());

                        break;
                    case Actions.KICK_MEMBER_FROM_CHAT_ROOM:
                        KickMemberFromChatRoom kickMemberFromChatRoom = (KickMemberFromChatRoom) o.getMessageObject();
                        // thông báo
                        Untilities.alert("Bạn đã bị người quản lý mời ra khỏi phòng chat!");
                        // thực hiện các thủ tục để người dùng rời phòng:
                        // 1. dispose
                        this.publicChatRoomList.get(Integer.valueOf(kickMemberFromChatRoom.getFromRoom().getRoomId())).dispose();
                        // 2. gửi thông báo rời phòng lên server
                        Untilities.sendTo(new SendMe(
                                Actions.USER_LEAVE_FROM_CHAT_ROOM,
                                new KickMemberFromChatRoom(user, user.getId(), user.getFullname(), kickMemberFromChatRoom.getFromRoom())
                        ), socket);
                        // 3. remove phòng chat từ publicChatRoomList
                        this.publicChatRoomList.remove(Integer.valueOf(kickMemberFromChatRoom.getFromRoom().getRoomId()));
                        break;
                    case Actions.USER_LEAVE_FROM_CHAT_ROOM:
                        WelcomeUserToChatRoom welcomeUserToLeaveRoom = (WelcomeUserToChatRoom) o.getMessageObject();
                        PublicChatRoomUI tempPublicChatRoomUI = this.publicChatRoomList
                                .get(Integer.valueOf(welcomeUserToLeaveRoom.getChatRoomMessage().getRoom().getRoomId()));
                        // load lại danh sách
                        tempPublicChatRoomUI.userMemberList = welcomeUserToLeaveRoom.getArrayListClient();
                        tempPublicChatRoomUI.loadMemberList();
                        // gửi tin nhắn trong phòng
                        tempPublicChatRoomUI.txtAreaChatRoom.append(welcomeUserToLeaveRoom.getChatRoomMessage().getMsg());
                        break;
                } // END - switch (o.getTypeOfMessage()) {...
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Lỗi: " + e);
        }
    }

}
