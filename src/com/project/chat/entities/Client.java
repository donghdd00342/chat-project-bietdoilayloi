/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;
import java.net.Socket;

/**
 *
 * @author DongHo
 */
public class Client implements Serializable{

     Socket socket;
     private User user;

     public Client() {
     }

     public Client(Socket socket) {
	  this.socket = socket;
     }

     public Client(Socket socket, User user) {
	  this.socket = socket;
	  this.user = user;
     }

     public User getUser() {
	  return user;
     }

     public void setUser(User user) {
	  this.user = user;
     }

     public Socket getSocket() {
	  return socket;
     }

     public void setSocket(Socket socket) {
	  this.socket = socket;
     }
}
