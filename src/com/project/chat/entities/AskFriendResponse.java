/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;

/**
 *
 * @author DongHo
 */
public class AskFriendResponse implements Serializable {

     private int responseResult;
     SendFriendRequest sendFriendRequest;

     public AskFriendResponse(int responseResult, SendFriendRequest sendFriendRequest) {
	  this.responseResult = responseResult;
	  this.sendFriendRequest = sendFriendRequest;
     }

     public int getResponseResult() {
	  return responseResult;
     }

     public void setResponseResult(int responseResult) {
	  this.responseResult = responseResult;
     }

     public SendFriendRequest getSendFriendRequest() {
	  return sendFriendRequest;
     }

     public void setSendFriendRequest(SendFriendRequest sendFriendRequest) {
	  this.sendFriendRequest = sendFriendRequest;
     }
}
