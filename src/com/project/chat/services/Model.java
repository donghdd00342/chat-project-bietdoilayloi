/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.services;

import com.project.chat.entities.ChatRoomsList;
import com.project.chat.entities.Client;
import com.project.chat.entities.GetHistoryPrivateChat;
import com.project.chat.entities.Room;
import com.project.chat.entities.OfflineMessages;
import com.project.chat.entities.SendFriendRequest;
import com.project.chat.entities.User;
import com.project.chat.entities.UserInformations;
import com.project.chat.services.model.ChatRoom;
import com.project.chat.services.model.Table;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author DongHo
 */
public class Model {

    private Model() {
    }

    // đánh dấu đã đọc tin nhắn
    public static void letReadOfflineMsg(OfflineMessages offlineMsg) {
        try {
            String sql = "UPDATE " + Setting.LOGS_TABLE_NAME + " SET status = 1 WHERE " + Setting.LOGS_TABLE_NAME + ".relationship_id = " + offlineMsg.getRelationshipId();
            System.out.println("[MySQL] sql = " + sql);
            Statement stt = DAO.dbConnect().createStatement();
            stt.execute(sql);
        } catch (SQLException e) {
            System.err.println("Lỗi đổi tin nhắn thành đã đọc: " + e);
        }
    }

    // lấy tất cả tin nhắn chưa đọc của user.id
    public static HashMap<Integer, OfflineMessages> getAllOfflineMsg(int friendId) {
        HashMap<Integer, OfflineMessages> listOfflineMsg = new HashMap<>();
        ArrayList<String> tempList;
        int fromId;
        String fromName;
        String content;
        int relationshipId;

        try {
            StringBuilder sql = new StringBuilder("SELECT * FROM ").append(Setting.LOGS_TABLE_NAME);
            sql.append(" JOIN ").append(Setting.RELATIONSHIPS_TABLE_NAME);
            sql.append(" ON ").append(Setting.LOGS_TABLE_NAME).append(".relationship_id = ").append(Setting.RELATIONSHIPS_TABLE_NAME).append(".id");
            sql.append(" JOIN ").append(Setting.USERS_TABLE_NAME);
            sql.append(" ON ").append(Setting.RELATIONSHIPS_TABLE_NAME).append(".sub_id = ").append(Setting.USERS_TABLE_NAME).append(".id");
            sql.append(" WHERE ").append(Setting.RELATIONSHIPS_TABLE_NAME).append(".friend_id = ").append("" + friendId);
            sql.append(" AND ").append(Setting.LOGS_TABLE_NAME).append(".status = 0");

            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql.toString());
            while (rs.next()) {
                fromId = rs.getInt(10);
                fromName = rs.getString(13);
                content = rs.getString(3);
                relationshipId = rs.getInt(6);

                if (listOfflineMsg.containsKey(Integer.valueOf(fromId))) {
                    // add:thêm [content+fromName] vào value:OfflineMessages -> ArrayList<String> msgList
                    listOfflineMsg.get(Integer.valueOf(fromId)).getMsgList().add(" [" + fromName + "] " + content + "\n");
                } else {
                    // put:thêm [fromId, tempOfflineMessages] vào listOfflineMsg
                    tempList = new ArrayList<>();
                    tempList.add(" [" + fromName + "] " + content + "\n");
                    listOfflineMsg.put(
                            Integer.valueOf(fromId),
                            new OfflineMessages(fromId, fromName, content, tempList, relationshipId)
                    );
                }
            }

        } catch (Exception e) {
            System.err.println("Lỗi lấy tin nhắn offline: " + e);
        }
        return listOfflineMsg;
    }

    // init chatRoomMap
    public static void initChatRoomMap(Map<Integer, ArrayList<Client>> chatRoomMap) {
        try {
            StringBuilder sql = new StringBuilder("select id from ").append(Setting.CHAT_ROOMS_TABLE_NAME);
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql.toString());
            while (rs.next()) {
                chatRoomMap.put(rs.getInt("id"), new ArrayList<>());
            }
        } catch (SQLException ex) {
            System.err.println("Lỗi init chatRoomMap@Model: " + ex);
        }
    }

    // Tạo room chat -> trả về Room hoặc Null
    public static Room createChatRoom(Room room) {
        // nếu tên phòng chưa có thì tạo mới phòng
        try {
            // kiểm tra sự tồn tại của tên phòng
            StringBuilder sql = new StringBuilder("select * from ");
            sql.append(Setting.CHAT_ROOMS_TABLE_NAME).append(" where name = '").append(room.getRoomName()).append("'");
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql.toString());
            if (rs.next()) {
                return null;
            } else {
                room.setRoomId(Model.insert(new ChatRoom(room.getOwnId(), room.getRoomName(), room.getRoomDescripton(), 1)));
                return room;
            }
        } catch (SQLException ex) {
            System.err.println("Lỗi tạo phòng chat@Model: " + ex);
            return null;
        }
    }

    // Lấy danh sách phòng chat
    public static ChatRoomsList getChatRoomList(User user) {
        ChatRoomsList chatRoomsList = new ChatRoomsList();
        try {
            // getAll
            StringBuilder sqlGetAll = new StringBuilder("select * from ");
            sqlGetAll.append(Setting.CHAT_ROOMS_TABLE_NAME).append(" JOIN ").append(Setting.USERS_TABLE_NAME);
            sqlGetAll.append(" on ").append(Setting.CHAT_ROOMS_TABLE_NAME).append(".own = ");
            sqlGetAll.append(Setting.USERS_TABLE_NAME).append(".id");
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sqlGetAll);
            ResultSet rs = stt.executeQuery(sqlGetAll.toString());
            while (rs.next()) {
                chatRoomsList.getAll().add(new Room(
                        rs.getInt(1),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(2),
                        rs.getString(9)
                ));
            }
            // getOfMine
            StringBuilder sqlGetOfMine = new StringBuilder(sqlGetAll);
            sqlGetOfMine.append(" where ").append(Setting.CHAT_ROOMS_TABLE_NAME).append(".own = ").append(Integer.toString(user.getId()));
            System.out.println("[MySQL] sql = " + sqlGetOfMine);
            rs = stt.executeQuery(sqlGetOfMine.toString());
            while (rs.next()) {
                chatRoomsList.getOfMine().add(new Room(
                        rs.getInt(1),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(2),
                        rs.getString(9)
                ));
            }
        } catch (SQLException ex) {
            System.err.println("Lỗi Lấy danh sách phòng chat: " + ex);
        }
        return chatRoomsList;
    }

    // Lấy lịch sử chat
    public static HashMap<Long, String> getHistoryPrivateChat(GetHistoryPrivateChat getHistoryPrivateChat) {
        HashMap<Long, String> listMsg = new HashMap<>();
        try {
            Statement stt = DAO.dbConnect().createStatement();
            ResultSet rs;
            String msgSent = "SELECT content, create_at FROM " + Setting.LOGS_TABLE_NAME + " WHERE relationship_id = " + Model.getRelationshipId(
                    getHistoryPrivateChat.getUserId(), getHistoryPrivateChat.getFriendId()) + " AND status != -1 LIMIT " + (getHistoryPrivateChat.getViewOption() == 0 ? (getHistoryPrivateChat.getLimitCount() / 2) : getHistoryPrivateChat.getLimitCount()); // chỉ từ bạn
            String msgReceive = "SELECT content, create_at FROM " + Setting.LOGS_TABLE_NAME + " WHERE relationship_id = " + Model.getRelationshipId(
                    getHistoryPrivateChat.getFriendId(), getHistoryPrivateChat.getUserId()) + " AND status != -1 LIMIT " + (getHistoryPrivateChat.getViewOption() == 0 ? (getHistoryPrivateChat.getLimitCount() / 2) : getHistoryPrivateChat.getLimitCount()); // chỉ từ bạn của bạn
            switch (getHistoryPrivateChat.getViewOption()) {
                case 0: // Tất cả
                    // từ người gửi
                    System.out.println("[MySQL] sql = " + msgSent);
                    rs = stt.executeQuery(msgSent);
                    while (rs.next()) {
                        listMsg.put(rs.getLong("create_at"), " [TÔI] " + rs.getString("content") + "\n");
                    }
                    // từ người nhận
                    System.out.println("[MySQL] sql = " + msgReceive);
                    rs = stt.executeQuery(msgReceive);
                    while (rs.next()) {
                        listMsg.put(rs.getLong("create_at"), " [" + getHistoryPrivateChat.getFriendName().trim() + "] " + rs.getString("content") + "\n");
                    }
                    // sắp xếp theo thứ tự -> đẩy về client giảm tải
                    break;
                case 1: // Chỉ từ bạn
                    System.out.println("[MySQL] sql = " + msgSent);
                    rs = stt.executeQuery(msgSent);
                    while (rs.next()) {
                        listMsg.put(rs.getLong("create_at"), " [TÔI] " + rs.getString("content") + "\n");
                    }
                    break;
                case 2: // Chỉ từ bạn của bạn
                    System.out.println("[MySQL] sql = " + msgReceive);
                    rs = stt.executeQuery(msgReceive);
                    while (rs.next()) {
                        listMsg.put(rs.getLong("create_at"), " [" + getHistoryPrivateChat.getFriendName() + "] " + rs.getString("content") + "\n");
                    }
                    break;
            }
        } catch (SQLException ex) {
            System.err.println("Lỗi lấy lịch sử tin nhắn: " + ex);
        }
        return listMsg;
    }

    // lấy relationship_id từ sub_id và friend_id;
    public static int getRelationshipId(int subId, int friendId) {
        try {
            // kiem tra trong databse
            String sql = "select id from " + Setting.RELATIONSHIPS_TABLE_NAME + " where sub_id = " + subId + " AND friend_id = " + friendId + ";";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);
            if (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return 0;
    }

    public static User checkEmailExisting(String email) {
        User user = new User();
        try {
            // kiem tra trong databse
            String sql = "select * from " + Setting.USERS_TABLE_NAME + " where email= '" + email + "';";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);
            if (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setMail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setFullname(rs.getString("full_name"));
                user.setToken(rs.getString("tocken"));
                user.setAccessCode(rs.getString("access_code"));
                user.setStatus(rs.getInt("access_code"));
            } else {
                return null;
            }

        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return user;
    }

    public static String changePasswordUser(UserInformations userInformations) {
        try {
            String sql = "UPDATE " + Setting.USERS_TABLE_NAME
                    + " SET password = '" + userInformations.getNewPass() + "'"
                    + " WHERE id = " + userInformations.getId() + ";";
            System.out.println("[MySQL] sql = " + sql);
            Statement stt = DAO.dbConnect().createStatement();
            stt.execute(sql);

        } catch (Exception e) {
            System.err.println("Lỗi thay đổi mật khẩu: " + e);
            return "Có lỗi xẩy ra tại server, đổi pass không thành công!";
        }
        return "Thay đổi mật khẩu thành công!";
    }

    public static String changeInfomationsUser(UserInformations userInformations) {
        try {
            String sql = "UPDATE " + Setting.USERS_TABLE_NAME
                    + " SET full_name = '" + userInformations.getFullName() + "'"
                    + " WHERE id = " + userInformations.getId() + ";";
            System.out.println("[MySQL] sql = " + sql);
            Statement stt = DAO.dbConnect().createStatement();
            stt.execute(sql);
        } catch (Exception e) {
            System.err.println("Lỗi cập nhật thông tin user: " + e);
            return "Có lỗi xẩy ra tại server, cập nhật không thành công!";
        }
        return "Cập nhật thành công!";
    }

    /**
     * trả về List các yêu cầu kết bạn của 1 id
     *
     * @param id
     * @return
     */
    public static ArrayList<SendFriendRequest> getFriendRequestListOfId(int id) {
        ArrayList<SendFriendRequest> listRequest = new ArrayList<>();
        try {
            String sql = "select * from " + Setting.RELATIONSHIPS_TABLE_NAME + " where sub_id = " + id + " and status = 0;";
            System.out.println("[MySQL] sql = " + sql);
            Statement stt = DAO.dbConnect().createStatement();
            ResultSet rs = stt.executeQuery(sql);

            if (rs.next()) {
                SendFriendRequest sendFriendRequest = new SendFriendRequest(id, id, sql, sql);
                listRequest.add(new SendFriendRequest(
                        rs.getInt("friend_id"),
                        id,
                        Model.getFriendRequestMessage(rs.getInt("friend_id"), id),
                        Model.getFullNameById(rs.getInt("friend_id"))
                ));
                return listRequest;
            } else {
                return listRequest;
            }

        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return listRequest;
    }

    /**
     * Lấy tin nhắn của yêu cầu kết bạn
     *
     * @param fromId
     * @param toId
     * @return
     */
    public static String getFriendRequestMessage(int fromId, int toId) {
        try {
            String sql = "select content from " + Setting.LOGS_TABLE_NAME + " where relationship_id= '" + Model.checkExistingRelationship(fromId, toId) + "' and status = -1 ORDER BY id DESC LIMIT 1;";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);

            if (rs.next()) {
                return rs.getString(1);
            } else {
                return "";
            }

        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return "";
    }

    /**
     * Lấy full name của một user.id
     *
     * @param id
     * @return
     */
    public static String getFullNameById(int id) {
        try {
            String sql = "select full_name from " + Setting.USERS_TABLE_NAME + " where id = " + id + ";";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);

            if (rs.next()) {
                return rs.getString(1);
            } else {
                return "";
            }

        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return "";
    }

    /**
     * Kiểm tra sự tồn tại của user với email và pass (đăng nhập)
     *
     * @param email
     * @param password
     * @return
     */
    public static User checkUserExisting(String email, String password) {
        User user = new User();
        try {
            // kiem tra trong databse, neu co user thi tra ve true , nguoc lai false.
            String sql = "select * from " + Setting.USERS_TABLE_NAME + " where email= '" + email + "' and password = '" + password + "';";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);

            if (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setMail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setFullname(rs.getString("full_name"));
                user.setToken(rs.getString("tocken"));
                user.setAccessCode(rs.getString("access_code"));
                user.setStatus(rs.getInt("access_code"));
            } else {
                return null;
            }

        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return user;
    }

    /**
     * Kiểm tra và trả về mối quan hệ của 2 user.id
     *
     * @param fromId
     * @param toId
     * @return
     */
    public static int checkExistingRelationship(int fromId, int toId) {
        try {
            String sql = "select id from " + Setting.RELATIONSHIPS_TABLE_NAME + " where sub_id= '" + fromId + "' and friend_id = '" + toId + "';";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);

            if (rs.next()) {
                return rs.getInt("id");
            } else {
                return 0;
            }

        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return 0;
    }

    public static int checkStatusRelationship(int fromId, int toId, int status) {
        try {
            String sql = "select id from " + Setting.RELATIONSHIPS_TABLE_NAME + " where sub_id= '" + fromId + "' and friend_id = '" + toId + "' and status = " + status + ";";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);

            if (rs.next()) {
                return rs.getInt("id");
            } else {
                System.out.println("Không tìm thấy relationship.status = " + status + " như yêu cầu!");
                return 0;
            }

        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return 0;
    }

    public static int checkStatusBlock(int fromId, int toId) {
        try {
            String sql = "select id from " + Setting.RELATIONSHIPS_TABLE_NAME + " where (sub_id= '" + fromId + "' and friend_id = '" + toId + "') and (status = -1 or status = -3);";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);

            if (rs.next()) {
                return rs.getInt("id");
            } else {
                System.out.println("Không tìm thấy relationship.status = -1 or -3 như yêu cầu!");
                return 0;
            }

        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return 0;
    }

    /**
     * Lấy id trong Logs của một mối quan hệ (chứa tin nhắn yêu cầu kết bạn)
     *
     * @param relationshipId
     * @return
     */
    public static int getLogIdOfFriendRequest(int relationshipId) {
        try {
            String sql = "select id from " + Setting.LOGS_TABLE_NAME + " where relationship_id= '" + relationshipId + "' and status = -1;";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);

            if (rs.next()) {
                return rs.getInt("id");
            } else {
                return 0;
            }

        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return 0;
    }

    /**
     * Tìm kiếm user theo full name
     *
     * @param keyword
     * @return
     */
    public static HashMap<Integer, String> searchUser(String keyword) {

        HashMap<Integer, String> userHashMapResult = new HashMap<>();

        try {
            // kiem tra trong databse, neu co user thi tra ve true , nguoc lai false.
            String sql = "select * from " + Setting.USERS_TABLE_NAME + " where full_name like '%" + keyword + "%';";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);

            while (rs.next()) {
                userHashMapResult.put(Integer.parseInt(rs.getString("id")), rs.getString("full_name"));
            }
        } catch (SQLException ex) {
            System.err.println("Lỗi: " + ex);
        }
        return userHashMapResult;
    }

    /**
     * Lấy danh sách bạn bè của một user.id
     *
     * @param id
     * @return
     */
    public static HashMap<Integer, String> getFriendList(int id) {
        HashMap<Integer, String> friendMap = new HashMap<>();
        int friend_id;
        String friend_fullname;

        try {
            String sql = "SELECT " + Setting.RELATIONSHIPS_TABLE_NAME + ".friend_id, " + Setting.USERS_TABLE_NAME + ".full_name FROM " + Setting.RELATIONSHIPS_TABLE_NAME + " JOIN " + Setting.USERS_TABLE_NAME + " ON " + Setting.RELATIONSHIPS_TABLE_NAME + ".friend_id = " + Setting.USERS_TABLE_NAME + ".id WHERE " + Setting.RELATIONSHIPS_TABLE_NAME + ".sub_id = " + id + " and " + Setting.RELATIONSHIPS_TABLE_NAME + ".status = 1";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);
            while (rs.next()) {
                friend_id = rs.getInt("friend_id");
                friend_fullname = rs.getString("full_name");
                friendMap.put(friend_id, friend_fullname);
            }
        } catch (SQLException e) {
            System.err.println("Lỗi lấy danh sách bạn: " + e);
        }
        return friendMap;
    }

    /**
     * Lấy danh sách người dùng bị chặn của một user.id
     *
     * @param id
     * @return
     */
    public static HashMap<Integer, String> getBlockList(int id) {
        HashMap<Integer, String> blockList = new HashMap<>();
        int friend_id;
        String friend_fullname;

        try {
            String sql = "SELECT " + Setting.RELATIONSHIPS_TABLE_NAME + ".friend_id, " + Setting.USERS_TABLE_NAME + ".full_name FROM " + Setting.RELATIONSHIPS_TABLE_NAME + " JOIN " + Setting.USERS_TABLE_NAME + " ON " + Setting.RELATIONSHIPS_TABLE_NAME + ".friend_id = " + Setting.USERS_TABLE_NAME + ".id WHERE " + Setting.RELATIONSHIPS_TABLE_NAME + ".sub_id = " + id + " and " + Setting.RELATIONSHIPS_TABLE_NAME + ".status = -1";
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] sql = " + sql);
            ResultSet rs = stt.executeQuery(sql);
            while (rs.next()) {
                friend_id = rs.getInt("friend_id");
                friend_fullname = rs.getString("full_name");
                blockList.put(friend_id, friend_fullname);
            }
        } catch (SQLException e) {
            System.err.println("Lỗi lấy danh sách người dùng bị chặn: " + e);
        }
        return blockList;
    }

    // CRUD //
    /**
     * Chèn một đối tượng Model vào bảng của nó (ko cần id)
     *
     * @param <T>
     * @param obj
     * @return
     * @throws SQLException
     */
    public static <T> int insert(T obj) throws SQLException {
        return update(obj);
    }

    /**
     * Cập nhật một đối tượng mới (cần id để xác định cập nhật thằng nào)
     *
     * @param <T>
     * @param obj
     * @return
     * @throws SQLException
     */
    public static <T> int update(T obj) throws SQLException {
        StringBuilder columns = new StringBuilder();
        StringBuilder values = new StringBuilder();
        StringBuilder sqlSet = new StringBuilder();
        int id = 0;
        int lastInsertId = 0;
        // get columns, values, sqlSet, id
        try {
            Field[] fields = obj.getClass().getDeclaredFields();
            String preColumn = "";
            String preValue = "'";
            for (Field f : fields) {
                f.setAccessible(true);
                // getId
                if (f.getName().equals("id")) {
                    id = (int) f.get(obj);
                    continue;
                }
                columns.append(preColumn).append(f.getName());
                sqlSet.append(preColumn).append(f.getName()).append(" = '").append(f.get(obj)).append("'");
                if (f.getType().toString().equals("boolean")) {
                    if (f.get(obj).toString().equals("true")) {
                        values.append(preValue).append("1");
                    } else {
                        values.append(preValue).append("0");
                    }
                } else {
                    values.append(preValue).append(f.get(obj));
                }
                preColumn = ", ";
                preValue = "', '";
            }
        } catch (IllegalAccessException | IllegalArgumentException | SecurityException e) {
            System.err.println("Lỗi: " + e);
        }
        values.append("'");
        StringBuilder querryString = new StringBuilder();
        if ((obj instanceof Table) && (columns.length() != 0) && (values.length() != 0)) {
            Table tb = (Table) obj;
            String table = tb.getTable();
            Statement stt = DAO.dbConnect().createStatement();
            // get querryString
            if (id <= 0) {
                querryString.append("INSERT INTO ").append(table).append(" (").append(columns).append(") VALUES(").append(values).append(");");
                System.out.println("[MySQL] querryString = " + querryString.toString()); // debug
                stt.executeUpdate(querryString.toString(), Statement.RETURN_GENERATED_KEYS);
                ResultSet rs = stt.getGeneratedKeys();
                if (rs.next()) {
                    lastInsertId = rs.getInt(1);
                }
            } else {
                querryString.append("UPDATE ").append(table).append(" SET ").append(sqlSet).append(" WHERE id = '").append(id).append("';");
                System.out.println("[MySQL] querryString = " + querryString.toString()); // debug
                stt.execute(querryString.toString());
                lastInsertId = id;
            }
        } else {
            System.err.println("Lỗi định dạng Model Object!");
        }
        return lastInsertId;
    }

    /**
     * Xóa một đối tượng nào đó trong bảng của nó theo id
     *
     * @param <T>
     * @param obj
     * @param id
     * @throws SQLException
     */
    public static <T> void delete(T obj, int id) throws SQLException {
        StringBuilder querryString = new StringBuilder();
        if (obj instanceof Table) {
            Table tb = (Table) obj;
            String table = tb.getTable();
            if (id > 0) {
                querryString.append("DELETE FROM ").append(table).append(" WHERE id = ").append(id);
            } else {
                System.err.println("id < 0! không tìm thấy id để xóa.");
            }
            Statement stt = DAO.dbConnect().createStatement();
            System.out.println("[MySQL] querryString = " + querryString.toString()); // debug
            stt.execute(querryString.toString());
        } else {
            System.err.println("Lỗi định dạng Model Object!");
        }
    }
}
