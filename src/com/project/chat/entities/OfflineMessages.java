/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author DongHo
 */
public class OfflineMessages implements Serializable {
     
     private int fromId; // subId
     private String fromName; // join user
     private String lastMsg;
     private ArrayList<String> msgList;
     int relationshipId; // để chuyển status của logs

     public OfflineMessages() {
	  // TODO Auto-generated constructor stub
     }

    public OfflineMessages(int fromId, String fromName, String lastMsg, ArrayList<String> msgList, int relationshipId) {
        this.fromId = fromId;
        this.fromName = fromName;
        this.lastMsg = lastMsg;
        this.msgList = msgList;
        this.relationshipId = relationshipId;
    }

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }

    public ArrayList<String> getMsgList() {
        return msgList;
    }

    public void setMsgList(ArrayList<String> msgList) {
        this.msgList = msgList;
    }

    public int getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(int relationshipId) {
        this.relationshipId = relationshipId;
    }
}
