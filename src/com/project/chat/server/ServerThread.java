/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.server;

import com.project.chat.entities.Client;
import com.project.chat.entities.LoginSuccessResponse;
import com.project.chat.entities.SendMe;
import com.project.chat.entities.Actions;
import com.project.chat.entities.AskFriendResponse;
import com.project.chat.entities.ChatRoomMessage;
import com.project.chat.entities.ExistingRelationship;
import com.project.chat.entities.GetHistoryPrivateChat;
import com.project.chat.entities.JoinToChatRoom;
import com.project.chat.entities.KickMemberFromChatRoom;
import com.project.chat.entities.LetReload;
import com.project.chat.entities.OfflineMessages;
import com.project.chat.entities.PrivateMessage;
import com.project.chat.entities.Room;
import com.project.chat.entities.SendFriendRequest;
import com.project.chat.entities.User;
import com.project.chat.entities.UserInformations;
import com.project.chat.entities.WelcomeUserToChatRoom;
import com.project.chat.services.Model;
import com.project.chat.services.Untilities;
import com.project.chat.services.model.ChatRoom;
import com.project.chat.services.model.Logs;
import com.project.chat.services.model.Relationship;
import com.project.chat.services.model.Users;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author DongHo
 */
public class ServerThread {

    private final Map<Integer, Client> clientsMap;
    private final Socket socket;

    // list phòng chat
    private final Map<Integer, ArrayList<Client>> chatRoomMap;

    boolean letCloseThread;

    public ServerThread(Socket socket, Map<Integer, Client> clientsMap, Map<Integer, ArrayList<Client>> chatRoomMap) {
        this.socket = socket;
        this.clientsMap = clientsMap;
        this.chatRoomMap = chatRoomMap;

        this.letCloseThread = false;
        System.out.println("Thread:" + socket.getInetAddress() + "/" + socket.getPort() + " = " + this);
    }

    //@Override
    public void run() {
        try {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String request = br.readLine();
                System.out.println("[ĐĂNG NHẬP] request = " + request);
                PrintStream ps = new PrintStream(socket.getOutputStream());
                boolean checkUserDatabase = true;

                String[] requestArray;
                String requestType;
                String fullName;
                String userEmail;
                String userPassword;
                if (request != null) {
                    // lấy thông tin
                    try {
                        requestArray = request.split(" ");
                        requestType = requestArray[0];
                        fullName = requestArray[1].replace('*', ' ');
                        userEmail = requestArray[2];
                        userPassword = requestArray[3];
                    } catch (Exception e) {
                        System.err.println("Lỗi: chuỗi gửi lên từ client ko đúng định dạng! " + e);
                        return;
                    }
                    switch (requestType) {
                        case "forget":
                            User myUser = Model.checkEmailExisting(userEmail);
                            if (myUser != null) {
                                // gửi mail
                                if (Untilities.sendEmailTo(myUser.getFullname(), myUser.getMail(), myUser.getPassword())) {
                                    ps.println("Hệ thống đã gửi mật khẩu qua email, vui lòng check mail để lấy mật khẩu.");
                                }
                            } else {
                                ps.println("Không tìm thấy Email!");
                            }
                            break;
                        case "signup":
                            // kiểm tra xem email đã tồn tại chưa?
                            if (Model.checkEmailExisting(userEmail) != null) {
                                ps.println("Email đã tồn tại! hãy sử dụng email khác!");
                            } else {
                                try {
                                    if (Model.insert(new Users(userEmail, userPassword, fullName, "", "", 1)) != 0) {
                                        ps.println("Đăng ký thành công! hãy đăng nhập để sử dụng chương trình.");
                                    } else {
                                        ps.println("Có gì đó ko đúng! hãy liên hệ với Admin.");
                                    }
                                } catch (SQLException e) {
                                    ps.println("Lỗi thêm thành viên! vui lòng thử lại sau.");
                                    System.err.println("Lỗi thêm thành viên mới: " + e);
                                }
                            }
                            break;
                        case "login":
                            // kiểm tra đăng nhập
                            for (Map.Entry<Integer, Client> entry : clientsMap.entrySet()) {
                                Client client = entry.getValue();
                                if (client.getUser().getMail().equalsIgnoreCase(userEmail)) {
                                    ps.println("Email này đang đăng nhập! bạn không thể đăng nhập lúc này...");
                                    checkUserDatabase = false;
                                    break;
                                }
                            }
                            // kiểm tra user trong database
                            if (checkUserDatabase) {
                                User user = Model.checkUserExisting(userEmail, userPassword);
                                if (user != null) {
                                    ps.println("ok");
                                    System.out.println("[SERVER] socket = " + socket);
                                    // tạo token
                                    String tocken = user.getId() + "" + System.currentTimeMillis();
                                    // tạo client
                                    Client client = new Client(socket, user);
                                    // lưu token và client vào clientsMap
                                    clientsMap.put(Integer.valueOf(user.getId()), client); //clientsMap.put(token, client);
                                    // gửi thông tin user về phía client
                                    // setToken
                                    user.setToken(tocken); // bỏ ko dùng nữa
                                    LoginSuccessResponse loginSuccessResponse = new LoginSuccessResponse();
                                    // lấy danh sách bạn của user
                                    HashMap<Integer, String> listFriend = Model.getFriendList(user.getId());
                                    loginSuccessResponse.setListFriend(listFriend);
                                    // lấy danh sách các tin nhắn chưa đọc
                                    loginSuccessResponse.setListOfflineMsg(Model.getAllOfflineMsg(user.getId()));
                                    // lấy danh sách lời mời kết bạn chưa xử lý
                                    ArrayList<SendFriendRequest> sendFriendRequestList = Model.getFriendRequestListOfId(user.getId());
                                    loginSuccessResponse.setSendFriendRequestList(sendFriendRequestList);
                                    // lấy danh sách chặn người dùng (blockList)
                                    HashMap<Integer, String> blockList = Model.getBlockList(user.getId());
                                    loginSuccessResponse.setBlockList(blockList);
                                    // .setUser
                                    loginSuccessResponse.setUser(user);
                                    // Bắn thông tin về cho người dùng
                                    Untilities.sendTo(new SendMe(Actions.LOGIN_SUCCESS, loginSuccessResponse), socket);
                                    System.out.println("[SERVER] Đã gửi (" + socket.getInetAddress() + ") thông tin đăng nhập thành công");
                                    ObjectInputStream is;
                                    SendMe o;
                                    while (!this.letCloseThread) {
                                        System.out.println("[WHILE] Đang chờ request...............................");
                                        is = new ObjectInputStream(socket.getInputStream());
                                        try {
                                            o = (SendMe) is.readObject();
//						       is.close();
                                            // Bắt đầu xử lý paket //
                                            switch (o.getTypeOfMessage()) {
                                                case Actions.CLIENT_LOGOUT:
                                                    User userLogout = (User) o.getMessageObject();
                                                    // Xóa client khỏi clientsMap
                                                    this.clientsMap.remove(Integer.valueOf(userLogout.getId()));
                                                    System.out.println("[FROM CLIENT]" + userLogout.getFullname() + " đã đăng xuất!");
                                                    this.letCloseThread = true;
                                                    break;
                                                case Actions.SEARCH_FRIENDS:
                                                    String keyword = (String) o.getMessageObject();
                                                    Untilities.sendTo(new SendMe(Actions.SEARCH_FRIENDS, Model.searchUser(keyword)), socket);
                                                    break;
                                                case Actions.SEND_FRIEND_REQUEST:
                                                    try {
                                                        SendFriendRequest sendFriendRequest = (SendFriendRequest) o.getMessageObject();
                                                        // kiểm tra (valid) trong relationship có fromId và toId chưa?
                                                        if (0 != Model.checkExistingRelationship(sendFriendRequest.getFromId(), sendFriendRequest.getToId())) {
                                                            // nếu relationship.status = 0 thì thông báo về nếu có rồi. Không thì chuyển status = 0 (từ -2) [-1: chặn]
                                                            int relationshipId = Model.checkStatusRelationship(sendFriendRequest.getFromId(), sendFriendRequest.getToId(), -2);
                                                            if (0 != relationshipId) {
                                                                // trường hợp đã kết bạn nhưng đã xóa bạn
                                                                // chuyển status = 0
                                                                if (relationshipId != 0) {
                                                                    Model.update(
                                                                            new Relationship(
                                                                                    relationshipId,
                                                                                    sendFriendRequest.getFromId(),
                                                                                    sendFriendRequest.getToId(),
                                                                                    0
                                                                            ));
                                                                    // thêm log kết bạn mới
                                                                    Model.insert(new Logs(
                                                                            relationshipId,
                                                                            sendFriendRequest.getMessage(),
                                                                            -1,
                                                                            System.currentTimeMillis()
                                                                    ));
                                                                } // end-1
                                                                relationshipId = Model.checkStatusRelationship(sendFriendRequest.getToId(), sendFriendRequest.getFromId(), -2);
                                                                // chuyển status = 0
                                                                if (relationshipId != 0) {
                                                                    Model.update(
                                                                            new Relationship(
                                                                                    relationshipId,
                                                                                    sendFriendRequest.getToId(),
                                                                                    sendFriendRequest.getFromId(),
                                                                                    0
                                                                            ));
                                                                    // thêm log kết bạn mới
                                                                    Model.insert(new Logs(
                                                                            relationshipId,
                                                                            sendFriendRequest.getMessage(),
                                                                            -1,
                                                                            System.currentTimeMillis()
                                                                    ));
                                                                } //end-2
                                                                // kiểm tra xem user có đang online ko, nếu đang online thì gửi yêu cầu kết bạn
                                                                if (Untilities.isUserOnline(sendFriendRequest.getToId(), clientsMap)) {
                                                                    Client toClient = Untilities.getClientFromId(sendFriendRequest.getToId(), clientsMap);
                                                                    Untilities.sendTo(new SendMe(Actions.SEND_FRIEND_REQUEST, sendFriendRequest), toClient.getSocket());
                                                                    System.out.println("[SYSTEM] Đã gửi [Actions.SEND_FRIEND_REQUEST] tới " + toClient.getUser().getFullname());
                                                                }
                                                            } else {
                                                                // trường hợp mới kết bạn
                                                                Untilities.sendTo(new SendMe(Actions.EXISTING_RELATIONSHIP, new ExistingRelationship("Hmmm...2 người đã là bạn hoặc là yêu cầu chưa được chấp nhận hoặc bạn đã bị vào danh sách đen :D")), client.getSocket());
                                                                System.out.println("[SYSTEM] Đã gửi [Actions.EXISTING_RELATIONSHIP] tới " + client.getUser().getFullname());
                                                            }
                                                        } else {
                                                            // nếu chưa có thì tạo relationship với status = 0 (2 chiều) và Thêm log 2 chiều với status = -1
                                                            Model.insert(new Logs(
                                                                    Model.insert(new Relationship(sendFriendRequest.getFromId(), sendFriendRequest.getToId(), 0)),
                                                                    sendFriendRequest.getMessage(),
                                                                    -1,
                                                                    System.currentTimeMillis()
                                                            ));
                                                            Model.insert(new Logs(
                                                                    Model.insert(new Relationship(sendFriendRequest.getToId(), sendFriendRequest.getFromId(), 0)),
                                                                    sendFriendRequest.getMessage(),
                                                                    -1,
                                                                    System.currentTimeMillis()
                                                            ));
                                                            // kiểm tra xem user có đang online ko, nếu đang online thì gửi yêu cầu kết bạn
                                                            if (Untilities.isUserOnline(sendFriendRequest.getToId(), clientsMap)) {
                                                                Client toClient = Untilities.getClientFromId(sendFriendRequest.getToId(), clientsMap);
                                                                Untilities.sendTo(new SendMe(Actions.SEND_FRIEND_REQUEST, sendFriendRequest),
                                                                        toClient.getSocket());
                                                                System.out.println("[SYSTEM] Đã gửi [Actions.SEND_FRIEND_REQUEST] tới " + toClient.getUser().getFullname());
                                                            }
                                                        }
                                                    } catch (SQLException e) {
                                                        System.err.println("Lỗi: " + e);
                                                    }
                                                    break;
                                                case Actions.ASK_FRIEND_RESPONSE:
                                                    try {
                                                        AskFriendResponse askFriendResponse = (AskFriendResponse) o.getMessageObject();
                                                        int relationshipId;
                                                        LetReload letReload;
                                                        Client toClient;
                                                        switch (askFriendResponse.getResponseResult()) {
                                                            case 1: // đồng ý
                                                                // chuyển status = 1 (2 chiều)
                                                                relationshipId = Model.checkExistingRelationship(
                                                                        askFriendResponse.getSendFriendRequest().getFromId(),
                                                                        askFriendResponse.getSendFriendRequest().getToId()
                                                                );
                                                                if (relationshipId != 0) {
                                                                    Model.update(
                                                                            new Relationship(
                                                                                    relationshipId,
                                                                                    askFriendResponse.getSendFriendRequest().getFromId(),
                                                                                    askFriendResponse.getSendFriendRequest().getToId(),
                                                                                    1
                                                                            ));
                                                                } // end-1
                                                                relationshipId = Model.checkExistingRelationship(
                                                                        askFriendResponse.getSendFriendRequest().getToId(),
                                                                        askFriendResponse.getSendFriendRequest().getFromId()
                                                                );
                                                                if (relationshipId != 0) {
                                                                    Model.update(
                                                                            new Relationship(
                                                                                    relationshipId,
                                                                                    askFriendResponse.getSendFriendRequest().getToId(),
                                                                                    askFriendResponse.getSendFriendRequest().getFromId(),
                                                                                    1
                                                                            ));
                                                                } //end-2
                                                                // gửi yêu cầu reloadFriendList và hasSentRequestList.remove(id)
                                                                letReload = new LetReload();
                                                                // FROM: nếu From online
                                                                if (Untilities.isUserOnline(askFriendResponse.getSendFriendRequest().getFromId(), clientsMap)) {
                                                                    letReload.setListFriend(Model.getFriendList(askFriendResponse.getSendFriendRequest().getFromId()));
                                                                    letReload.setTempId(askFriendResponse.getSendFriendRequest().getFromId());
                                                                    letReload.setSendFriendRequestList(Model.getFriendRequestListOfId(
                                                                            askFriendResponse.getSendFriendRequest().getFromId()
                                                                    ));
                                                                    toClient = Untilities.getClientFromId(askFriendResponse.getSendFriendRequest().getFromId(), clientsMap);
                                                                    Untilities.sendTo(
                                                                            new SendMe(Actions.Update.FRIENDS_LIST_BEFORE_AGREE, letReload),
                                                                            toClient.getSocket()
                                                                    );
                                                                    System.out.println("[SYSTEM] Đã gửi [Actions.Update.FRIENDS_LIST_BEFORE_AGREE] tới " + toClient.getUser().getFullname());
                                                                }

                                                                // TO: nếu To online
                                                                if (Untilities.isUserOnline(askFriendResponse.getSendFriendRequest().getToId(), clientsMap)) {
                                                                    letReload.setListFriend(Model.getFriendList(askFriendResponse.getSendFriendRequest().getToId()));
                                                                    letReload.setTempId(askFriendResponse.getSendFriendRequest().getToId());
                                                                    letReload.setSendFriendRequestList(Model.getFriendRequestListOfId(
                                                                            askFriendResponse.getSendFriendRequest().getToId()
                                                                    ));
                                                                    toClient = Untilities.getClientFromId(askFriendResponse.getSendFriendRequest().getToId(), clientsMap);
                                                                    Untilities.sendTo(
                                                                            new SendMe(Actions.Update.FRIENDS_LIST_BEFORE_AGREE, letReload),
                                                                            toClient.getSocket()
                                                                    );
                                                                    System.out.println("[SYSTEM] Đã gửi [Actions.Update.FRIENDS_LIST_BEFORE_AGREE] tới " + toClient.getUser().getFullname());
                                                                }
                                                                break;
                                                            case 2: // từ chối -> chuyển status từ 0 sang -2
                                                                // chuyển status = -2 (2 chiều)
                                                                relationshipId = Model.checkStatusRelationship(
                                                                        askFriendResponse.getSendFriendRequest().getFromId(),
                                                                        askFriendResponse.getSendFriendRequest().getToId(),
                                                                        0
                                                                );
                                                                if (relationshipId != 0) {
                                                                    Model.update(
                                                                            new Relationship(
                                                                                    relationshipId,
                                                                                    askFriendResponse.getSendFriendRequest().getFromId(),
                                                                                    askFriendResponse.getSendFriendRequest().getToId(),
                                                                                    -2
                                                                            ));
                                                                } // end-1
                                                                relationshipId = Model.checkStatusRelationship(
                                                                        askFriendResponse.getSendFriendRequest().getToId(),
                                                                        askFriendResponse.getSendFriendRequest().getFromId(),
                                                                        0
                                                                );
                                                                if (relationshipId != 0) {
                                                                    Model.update(
                                                                            new Relationship(
                                                                                    relationshipId,
                                                                                    askFriendResponse.getSendFriendRequest().getToId(),
                                                                                    askFriendResponse.getSendFriendRequest().getFromId(),
                                                                                    -2
                                                                            ));
                                                                } //end-2
                                                                // nếu FROM online thì yêu cầu From remove id lưu trong list kết bạn
                                                                if (Untilities.isUserOnline(askFriendResponse.getSendFriendRequest().getFromId(), clientsMap)) {
                                                                    letReload = new LetReload();
                                                                    letReload.setTempId(askFriendResponse.getSendFriendRequest().getToId());
                                                                    letReload.setTempMessage(
                                                                            Model.getFullNameById(askFriendResponse.getSendFriendRequest().getToId()) + " đã từ chối kết bạn!"
                                                                    );
                                                                    toClient = Untilities.getClientFromId(askFriendResponse.getSendFriendRequest().getFromId(), clientsMap);
                                                                    Untilities.sendTo(
                                                                            new SendMe(Actions.REFUSE_FRIEND_REQUEST, letReload),
                                                                            toClient.getSocket()
                                                                    );
                                                                    System.out.println("[SYSTEM] Đã gửi [Actions.REFUSE_FRIEND_REQUEST] tới " + toClient.getUser().getFullname());
                                                                }
                                                                // To: (trường hợp xử lý sau) nếu To online thì To phải cập nhật lại danh sách requestFriend
                                                                if (Untilities.isUserOnline(askFriendResponse.getSendFriendRequest().getToId(), clientsMap)) {
                                                                    letReload = new LetReload();
                                                                    letReload.setSendFriendRequestList(Model.getFriendRequestListOfId(
                                                                            askFriendResponse.getSendFriendRequest().getToId()
                                                                    ));
                                                                    letReload.setBlockList(Model.getBlockList(askFriendResponse.getSendFriendRequest().getToId()));
                                                                    toClient = Untilities.getClientFromId(askFriendResponse.getSendFriendRequest().getToId(), clientsMap);
                                                                    Untilities.sendTo(
                                                                            new SendMe(Actions.Update.REQUEST_FRIEND_LIST_BEFORE_REFUSE, letReload),
                                                                            toClient.getSocket()
                                                                    );
                                                                    System.out.println("[SYSTEM] Đã gửi [Actions.Update.REQUEST_FRIEND_LIST_BEFORE_REFUSE] tới " + toClient.getUser().getFullname());
                                                                }
                                                                break;
                                                            case 3: // Chặn người dùng
                                                                // chuyển status = -1 (2 chiều)
                                                                relationshipId = Model.checkStatusRelationship(
                                                                        askFriendResponse.getSendFriendRequest().getFromId(),
                                                                        askFriendResponse.getSendFriendRequest().getToId(),
                                                                        0
                                                                );
                                                                if (relationshipId != 0) {
                                                                    Model.update(
                                                                            new Relationship(
                                                                                    relationshipId,
                                                                                    askFriendResponse.getSendFriendRequest().getFromId(),
                                                                                    askFriendResponse.getSendFriendRequest().getToId(),
                                                                                    -3
                                                                            ));
                                                                } // end-1
                                                                relationshipId = Model.checkStatusRelationship(
                                                                        askFriendResponse.getSendFriendRequest().getToId(),
                                                                        askFriendResponse.getSendFriendRequest().getFromId(),
                                                                        0
                                                                );
                                                                if (relationshipId != 0) {
                                                                    Model.update(
                                                                            new Relationship(
                                                                                    relationshipId,
                                                                                    askFriendResponse.getSendFriendRequest().getToId(),
                                                                                    askFriendResponse.getSendFriendRequest().getFromId(),
                                                                                    -1
                                                                            ));
                                                                } //end-2
                                                                // To: (trường hợp xử lý sau) nếu To online thì To phải cập nhật lại danh sách requestFriend
                                                                if (Untilities.isUserOnline(askFriendResponse.getSendFriendRequest().getToId(), clientsMap)) {
                                                                    letReload = new LetReload();
                                                                    letReload.setSendFriendRequestList(Model.getFriendRequestListOfId(
                                                                            askFriendResponse.getSendFriendRequest().getToId()
                                                                    ));
                                                                    // và cập nhật danh sách chặn
                                                                    letReload.setBlockList(Model.getBlockList(askFriendResponse.getSendFriendRequest().getToId()));
                                                                    toClient = Untilities.getClientFromId(askFriendResponse.getSendFriendRequest().getToId(), clientsMap);
                                                                    Untilities.sendTo(
                                                                            new SendMe(Actions.Update.REQUEST_FRIEND_LIST_BEFORE_REFUSE, letReload),
                                                                            toClient.getSocket()
                                                                    );
                                                                    System.out.println("[SYSTEM] Đã gửi [Actions.Update.REQUEST_FRIEND_LIST_BEFORE_REFUSE] tới " + toClient.getUser().getFullname());
                                                                }
                                                                break;
                                                            case 4: // lúc khác - xử lý sau
                                                                // gửi yêu cầu làm mới list yêu cầu kết bạn
                                                                if (Untilities.isUserOnline(askFriendResponse.getSendFriendRequest().getToId(), clientsMap)) {
                                                                    toClient = Untilities.getClientFromId(askFriendResponse.getSendFriendRequest().getToId(), clientsMap);
                                                                    letReload = new LetReload();
                                                                    letReload.setSendFriendRequestList(
                                                                            Model.getFriendRequestListOfId(askFriendResponse.getSendFriendRequest().getToId())
                                                                    );
                                                                    Untilities.sendTo(
                                                                            new SendMe(Actions.Update.FRIEND_REQUEST_FLOW_ADD_FRIEND, letReload),
                                                                            toClient.getSocket()
                                                                    );
                                                                    System.out.println("[SYSTEM] Đã gửi [Actions.Update.FRIEND_REQUEST_FLOW_ADD_FRIEND] tới " + toClient.getUser().getFullname());
                                                                }
                                                                break;
                                                        }
                                                    } catch (SQLException e) {
                                                        System.err.println("Lỗi xử lý kết bạn: " + e);
                                                    }
                                                    break;
                                                case Actions.USER_DELETE_FRIEND:
                                                    try {
                                                        SendFriendRequest sendFriendRequest = (SendFriendRequest) o.getMessageObject();
                                                        // chuyển status = -2 (2 chiều; từ 1 sang -2: hủy kết bạn)
                                                        int relationshipId = Model.checkStatusRelationship(
                                                                sendFriendRequest.getFromId(),
                                                                sendFriendRequest.getToId(),
                                                                1
                                                        );
                                                        if (relationshipId != 0) {
                                                            Model.update(
                                                                    new Relationship(
                                                                            relationshipId,
                                                                            sendFriendRequest.getFromId(),
                                                                            sendFriendRequest.getToId(),
                                                                            -2
                                                                    ));
                                                        } // end-1
                                                        relationshipId = Model.checkStatusRelationship(
                                                                sendFriendRequest.getToId(),
                                                                sendFriendRequest.getFromId(),
                                                                1
                                                        );
                                                        if (relationshipId != 0) {
                                                            Model.update(
                                                                    new Relationship(
                                                                            relationshipId,
                                                                            sendFriendRequest.getToId(),
                                                                            sendFriendRequest.getFromId(),
                                                                            -2
                                                                    ));
                                                        } //end-2
                                                        LetReload letReload = new LetReload();
                                                        Client toClient;
                                                        // FROM: nếu FROM online thì yêu cầu From cập nhật lại danh sách bạn
                                                        if (Untilities.isUserOnline(sendFriendRequest.getFromId(), clientsMap)) {
                                                            letReload.setTempMessage(
                                                                    Model.getFullNameById(sendFriendRequest.getToId()) + " đã từ chối kết bạn!"
                                                            );
                                                            letReload.setListFriend(Model.getFriendList(sendFriendRequest.getFromId()));
                                                            toClient = Untilities.getClientFromId(sendFriendRequest.getFromId(), clientsMap);
                                                            Untilities.sendTo(
                                                                    new SendMe(Actions.USER_DELETE_FRIEND, letReload),
                                                                    toClient.getSocket()
                                                            );
                                                            System.out.println("[SYSTEM] Đã gửi [Actions.USER_DELETE_FRIEND] tới " + toClient.getUser().getFullname());
                                                        }
                                                        // To: nếu To online thì To phải cập nhật lại danh sách bạn
                                                        if (Untilities.isUserOnline(sendFriendRequest.getToId(), clientsMap)) {
                                                            letReload.setTempMessage(
                                                                    "Bạn đã từ chối kết bạn với " + sendFriendRequest.getFromFullName() + "!"
                                                            );
                                                            letReload.setListFriend(Model.getFriendList(sendFriendRequest.getToId()));
                                                            toClient = Untilities.getClientFromId(sendFriendRequest.getToId(), clientsMap);
                                                            Untilities.sendTo(
                                                                    new SendMe(Actions.USER_DELETE_FRIEND, letReload),
                                                                    toClient.getSocket()
                                                            );
                                                            System.out.println("[SYSTEM] Đã gửi [Actions.USER_DELETE_FRIEND] tới " + toClient.getUser().getFullname());
                                                        }
                                                    } catch (SQLException e) {
                                                        System.err.println("Lỗi xử lý xóa bạn: " + e);
                                                    }
                                                    break;
                                                case Actions.USER_SET_UNBLOCK:
                                                    try {
                                                        SendFriendRequest sendFriendRequest = (SendFriendRequest) o.getMessageObject();
                                                        // thay đổi status của relationship từ -1 sang -2 (-1: chặn)
                                                        int relationshipId = Model.checkStatusBlock(
                                                                sendFriendRequest.getFromId(),
                                                                sendFriendRequest.getToId()
                                                        );
                                                        if (relationshipId != 0) {
                                                            Model.update(
                                                                    new Relationship(
                                                                            relationshipId,
                                                                            sendFriendRequest.getFromId(),
                                                                            sendFriendRequest.getToId(),
                                                                            -2
                                                                    ));
                                                        } // end-1
                                                        relationshipId = Model.checkStatusBlock(
                                                                sendFriendRequest.getToId(),
                                                                sendFriendRequest.getFromId()
                                                        );
                                                        if (relationshipId != 0) {
                                                            Model.update(
                                                                    new Relationship(
                                                                            relationshipId,
                                                                            sendFriendRequest.getToId(),
                                                                            sendFriendRequest.getFromId(),
                                                                            -2
                                                                    ));
                                                        } //end-2
                                                        LetReload letReload = new LetReload();
                                                        Client toClient;
                                                        // TO: nếu To online thì gửi thông báo và yêu cầu làm mới danh sách tới To
                                                        if (Untilities.isUserOnline(sendFriendRequest.getToId(), clientsMap)) {
                                                            letReload.setTempMessage(
                                                                    "Bạn đã UnBlock " + sendFriendRequest.getFromFullName() + "!"
                                                            );
                                                            letReload.setBlockList(Model.getBlockList(sendFriendRequest.getToId()));
                                                            toClient = Untilities.getClientFromId(sendFriendRequest.getToId(), clientsMap);
                                                            Untilities.sendTo(
                                                                    new SendMe(Actions.USER_SET_UNBLOCK, letReload),
                                                                    toClient.getSocket()
                                                            );
                                                            System.out.println("[SYSTEM] Đã gửi [Actions.USER_SET_UNBLOCK] tới " + toClient.getUser().getFullname());
                                                        }
                                                        // FROM: nếu From online thì remove id của To trong list kết bạn local
                                                        if (Untilities.isUserOnline(sendFriendRequest.getFromId(), clientsMap)) {
                                                            letReload = new LetReload();
                                                            letReload.setTempId(sendFriendRequest.getToId());
                                                            letReload.setTempMessage("");
                                                            toClient = Untilities.getClientFromId(sendFriendRequest.getFromId(), clientsMap);
                                                            Untilities.sendTo(
                                                                    new SendMe(Actions.REFUSE_FRIEND_REQUEST, letReload),
                                                                    toClient.getSocket()
                                                            );
                                                            System.out.println("[SYSTEM] Đã gửi [Actions.REFUSE_FRIEND_REQUEST] tới " + toClient.getUser().getFullname());
                                                        }
                                                    } catch (SQLException e) {
                                                        System.err.println("Lỗi xử lý UnBlock bạn: " + e);
                                                    }
                                                    break;
                                                case Actions.Update.USER_UPDATE_INFORMATIONS:
                                                    UserInformations userInformations = (UserInformations) o.getMessageObject();
                                                    Client toClient = Untilities.getClientFromId(userInformations.getId(), this.clientsMap);
                                                    Untilities.sendTo(new SendMe(
                                                            Actions.Update.USER_UPDATE_INFORMATIONS_RESPONSE,
                                                            Model.changeInfomationsUser(userInformations)
                                                    ), toClient.getSocket());
                                                    System.out.println("[SYSTEM] Đã gửi [Actions.Update.USER_UPDATE_INFORMATIONS_RESPONSE] tới " + toClient.getUser().getFullname());
                                                    break;
                                                case Actions.Update.USER_UPDATE_PASSWORD:
                                                    UserInformations userUpdatePass = (UserInformations) o.getMessageObject();
                                                    Client sendClient = Untilities.getClientFromId(userUpdatePass.getId(), this.clientsMap);
                                                    if (Model.checkUserExisting(userUpdatePass.getEmail(), userUpdatePass.getCurrentPass()) != null) {
                                                        Untilities.sendTo(new SendMe(
                                                                Actions.Update.USER_UPDATE_INFORMATIONS_RESPONSE,
                                                                Model.changePasswordUser(userUpdatePass)
                                                        ), sendClient.getSocket());
                                                        System.out.println("[SYSTEM] Đã gửi [Actions.Update.USER_UPDATE_INFORMATIONS_RESPONSE] tới " + sendClient.getUser().getFullname());
                                                    } else {
                                                        Untilities.sendTo(new SendMe(
                                                                Actions.Update.USER_UPDATE_INFORMATIONS_RESPONSE,
                                                                "Mật khẩu hiện tại không chính xác!"
                                                        ), sendClient.getSocket());
                                                        System.out.println("[SYSTEM] Đã gửi [Actions.Update.USER_UPDATE_INFORMATIONS_RESPONSE] tới " + sendClient.getUser().getFullname());
                                                    }
                                                    break;
                                                case Actions.SEND_PRIVATE_MESSAGE:
                                                    try {
                                                        PrivateMessage privateMessage = (PrivateMessage) o.getMessageObject();
                                                        // kiểm tra xem thằng nhận có online ko? online thì gửi cho nó, ko thì gửi offline, Logs.status = 0
                                                        if (Untilities.isUserOnline(privateMessage.getToId(), this.clientsMap)) {
                                                            Client sendPriMsgToClient = Untilities.getClientFromId(privateMessage.getToId(), this.clientsMap);
                                                            Untilities.sendTo(o, sendPriMsgToClient.getSocket());
                                                            // lưu tin nhắn vào Logs
                                                            int relationshipId = Model.checkStatusRelationship(
                                                                    privateMessage.getFromId(),
                                                                    privateMessage.getToId(),
                                                                    1
                                                            );
                                                            if (relationshipId != 0) {
                                                                Model.insert(new Logs(
                                                                        relationshipId,
                                                                        privateMessage.getPrivateMessage(),
                                                                        1,
                                                                        System.currentTimeMillis()
                                                                ));

                                                            }
                                                        } else {
                                                            // gửi tin offline cho thằng nhận
                                                            System.out.println("Gửi offline cho thằng nhận");
                                                            int relationshipId = Model.checkStatusRelationship(
                                                                    privateMessage.getFromId(),
                                                                    privateMessage.getToId(),
                                                                    1
                                                            );
                                                            if (relationshipId != 0) {
                                                                Model.insert(new Logs(
                                                                        relationshipId,
                                                                        privateMessage.getPrivateMessage(),
                                                                        0,
                                                                        System.currentTimeMillis()
                                                                ));

                                                            }
                                                        }
                                                    } catch (SQLException e) {
                                                        System.err.println("Lỗi gửi nhận private message: " + e);
                                                    }
                                                    break;
                                                case Actions.GET_HISTORY_PRIVATE_CHAT:
                                                    try {
                                                        GetHistoryPrivateChat getHistoryPrivateChat = (GetHistoryPrivateChat) o.getMessageObject();
                                                        getHistoryPrivateChat.setListMsg(Model.getHistoryPrivateChat(getHistoryPrivateChat));
                                                        Untilities.sendTo(new SendMe(Actions.GET_HISTORY_PRIVATE_CHAT, getHistoryPrivateChat), socket);

                                                    } catch (Exception e) {
                                                        System.err.println("Lỗi lấy lịch sử chát riêng: " + e);
                                                    }
                                                    break;
                                                case Actions.GET_CHAT_ROOM_LIST:
                                                    User userGetChatRoomList = (User) o.getMessageObject();
                                                    Client clientGetChatRoomList = Untilities.getClientFromId(userGetChatRoomList.getId(), this.clientsMap);
                                                    Untilities.sendTo(new SendMe(Actions.GET_CHAT_ROOM_LIST,
                                                            Model.getChatRoomList(userGetChatRoomList)
                                                    ), clientGetChatRoomList.getSocket());
                                                    break;
                                                case Actions.CREATE_CHAT_ROOM:
                                                    Room room = (Room) o.getMessageObject();
                                                    Client clientCreateChatRoom = Untilities.getClientFromId(room.getOwnId(), this.clientsMap);
                                                    Room returnRoom = Model.createChatRoom(room);
                                                    Untilities.sendTo(new SendMe(Actions.CREATE_CHAT_ROOM, returnRoom), clientCreateChatRoom.getSocket());
                                                    // thêm vào danh sách live
                                                    if (returnRoom != null) {
                                                        chatRoomMap.put(returnRoom.getRoomId(), new ArrayList<>());
                                                    }
                                                    break;
                                                case Actions.DELETE_CHAT_ROOM:
                                                    Room roomDelete = (Room) o.getMessageObject();
                                                    Client clientDeleteChatRoom = Untilities.getClientFromId(roomDelete.getOwnId(), this.clientsMap);
                                                    // kiểm tra xem room có người trong đó ko, có thì ko thể xóa
                                                    if (chatRoomMap.get(Integer.valueOf(roomDelete.getRoomId())).size() > 0) {
                                                        Untilities.sendTo(new SendMe(Actions.DELETE_CHAT_ROOM, null), clientDeleteChatRoom.getSocket());
                                                    } else {
                                                        try {
                                                            Model.delete(new ChatRoom(), roomDelete.getRoomId());
                                                            Untilities.sendTo(new SendMe(Actions.DELETE_CHAT_ROOM, roomDelete), clientDeleteChatRoom.getSocket());
                                                            chatRoomMap.remove(Integer.valueOf(roomDelete.getRoomId())); // xóa live
                                                        } catch (SQLException e) {
                                                            System.err.println("Lỗi xóa phòng chát:" + e);
                                                            Untilities.sendTo(new SendMe(Actions.DELETE_CHAT_ROOM, null), clientDeleteChatRoom.getSocket());
                                                        }
                                                    }
                                                    break;
                                                case Actions.JOIN_TO_CHAT_ROOM:
                                                    JoinToChatRoom joinToChatRoom = (JoinToChatRoom) o.getMessageObject();
                                                    // lấy danh sách arrayListClient
                                                    ArrayList<Client> arrayListClient = chatRoomMap.get(Integer.valueOf(joinToChatRoom.getRoom().getRoomId()));
                                                    // thêm client vào arrayListClient
                                                    if (arrayListClient != null) {
                                                        arrayListClient.add(client);
                                                    }
                                                    // đổi từ arrayListClient sang userList(memberList)
                                                    ArrayList<User> memberList = Untilities.convertListClient(arrayListClient);
                                                    // chuyển list user vào joinToChatRoom
                                                    joinToChatRoom.setMemberList(memberList);
                                                    Untilities.sendTo(new SendMe(
                                                            Actions.JOIN_TO_CHAT_ROOM, joinToChatRoom), socket);
                                                    // gửi thông báo user gia nhập phòng chat đến mọi người trong phòng
                                                    Untilities.sendTo(new SendMe(
                                                            Actions.WELCOME_USER_TO_CHAT_ROOM,
                                                            new WelcomeUserToChatRoom(
                                                                    new ChatRoomMessage(
                                                                            user,
                                                                            " [SYSTEM] Chào mừng ~" + user.getFullname() + "~ đã tham gia phòng chát \n",
                                                                            joinToChatRoom.getRoom()
                                                                    ),
                                                                    memberList
                                                            )
                                                    ), chatRoomMap.get(Integer.valueOf(joinToChatRoom.getRoom().getRoomId())));
                                                    break;
                                                case Actions.SEND_MESSAGE_TO_CHAT_ROOM:
                                                    ChatRoomMessage chatRoomMessage = (ChatRoomMessage) o.getMessageObject();
                                                    // gửi tin nhắn đến mọi người trong phòng và hiển thị tại phòng
                                                    Untilities.sendTo(
                                                            new SendMe(Actions.SEND_MESSAGE_TO_CHAT_ROOM, chatRoomMessage),
                                                            chatRoomMap.get(Integer.valueOf(chatRoomMessage.getRoom().getRoomId()))
                                                    );
                                                    break;
                                                case Actions.KICK_MEMBER_FROM_CHAT_ROOM:
                                                    KickMemberFromChatRoom kickMemberFromChatRoom = (KickMemberFromChatRoom) o.getMessageObject();
                                                    ArrayList<Client> tempArrayListClient = this.chatRoomMap.get(Integer.valueOf(kickMemberFromChatRoom.getFromRoom().getRoomId()));
                                                    Client targetClient = null;
                                                    for (Client t : tempArrayListClient) {
                                                        if (t.getUser().getId() == kickMemberFromChatRoom.getTargetId()) {
                                                            targetClient = t;
                                                            break;
                                                        }
                                                    }
                                                    // thông báo đến mọi người trong room đó biết là đối tượng sẽ bị kick
                                                    Untilities.sendTo(
                                                            new SendMe(
                                                                    Actions.SEND_MESSAGE_TO_CHAT_ROOM,
                                                                    new ChatRoomMessage(
                                                                            kickMemberFromChatRoom.getFromUser(),
                                                                            " [SYSTEM] Thông báo: ~" + kickMemberFromChatRoom.getTargerName() + "~ đã bị mời ra khỏi phòng \n",
                                                                            kickMemberFromChatRoom.getFromRoom()
                                                                    )
                                                            ),
                                                            chatRoomMap.get(Integer.valueOf(kickMemberFromChatRoom.getFromRoom().getRoomId()))
                                                    );
                                                    if (targetClient != null) {
                                                        // thông báo đến thành viên đó biết là mình đã bị kick
                                                        Untilities.sendTo(new SendMe(Actions.KICK_MEMBER_FROM_CHAT_ROOM, kickMemberFromChatRoom), targetClient.getSocket());
                                                        // xóa thành viên khỏi phòng chat live
                                                        tempArrayListClient.remove(targetClient);
                                                    }
                                                    break;
                                                case Actions.USER_LEAVE_FROM_CHAT_ROOM:
                                                    KickMemberFromChatRoom kickMemberWhenLeaveFromChatRoom = (KickMemberFromChatRoom) o.getMessageObject();
                                                    ArrayList<Client> tempArrayListClientUserLeave = this.chatRoomMap.get(Integer.valueOf(kickMemberWhenLeaveFromChatRoom.getFromRoom().getRoomId()));
                                                    Client fromClient = null;
                                                    for (Client t : tempArrayListClientUserLeave) {
                                                        if (t.getUser().getId() == kickMemberWhenLeaveFromChatRoom.getTargetId()) {
                                                            fromClient = t;
                                                            break;
                                                        }
                                                    }
                                                    // xóa user khỏi phòng chat live
                                                    tempArrayListClientUserLeave.remove(fromClient);
                                                    // thông báo đến mọi người trong room đó biết
                                                    Untilities.sendTo(
                                                            new SendMe(
                                                                    Actions.USER_LEAVE_FROM_CHAT_ROOM,
                                                                    new WelcomeUserToChatRoom(
                                                                            new ChatRoomMessage(
                                                                                    user,
                                                                                    " [SYSTEM] Thông báo: ~" + user.getFullname() + "~ đã rời phòng chát \n",
                                                                                    kickMemberWhenLeaveFromChatRoom.getFromRoom()
                                                                            ),
                                                                            Untilities.convertListClient(tempArrayListClientUserLeave)
                                                                    )
                                                            ),
                                                            chatRoomMap.get(Integer.valueOf(kickMemberWhenLeaveFromChatRoom.getFromRoom().getRoomId()))
                                                    );
                                                    break;
                                                case Actions.READ_OFFLINE_MSG:
                                                    OfflineMessages offlineMsg = (OfflineMessages) o.getMessageObject();
                                                    // chuyển status trong logs sang 1
                                                    Model.letReadOfflineMsg(offlineMsg);
                                                    break;
                                            } // END - switch (o.getTypeOfMessage())...
                                        } catch (ClassNotFoundException ex) {
                                            System.err.println("Lỗi (từ 110@while chờ TCP) : " + ex);
                                        }
                                        // kết thúc xử lý packet //
                                    }
                                    // END - CHỜ ĐỢI REQUEST//
                                } else {
                                    ps.println("Thông tin đăng nhập không đúng!");
                                }
                            } // END - case "login"
                            break;
                    } // END - switch (requestType)
                }
            } catch (IOException e) {
                System.err.println("Lỗi (ở run@ServerThread): " + e);
            }
            System.out.println("Kết thúc Thread: " + socket.getInetAddress() + " / " + socket.getPort());
            System.out.println("Đóng socket");
            this.socket.close();
        } catch (IOException ex) {
            System.out.println("Lỗi close socket: " + ex);
        }
    } // END - run()

}
