/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this
template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.client;

import com.project.chat.client.ui.LoginUI;
import java.io.IOException;

/**
 *
 * @author DongHo
 */
public class RunClient {

     public static void main(String[] args) throws IOException {
	  try {
	       for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
		    if ("Windows".equals(info.getName())) {
			 javax.swing.UIManager.setLookAndFeel(info.getClassName());
			 break;
		    }
	       }
	  } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
	       System.err.println("Lỗi: " + ex);
	  }
	  LoginUI.getInstance().setVisible(true);
     }

}
