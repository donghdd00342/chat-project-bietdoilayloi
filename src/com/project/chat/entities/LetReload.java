/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author DongHo
 */
public class LetReload implements Serializable {

     private HashMap<Integer, String> listFriend;
     private HashMap<Integer, String> blockList;
     private ArrayList<SendFriendRequest> sendFriendRequestList;
     private int tempId;
     private String tempMessage;

     public HashMap<Integer, String> getListFriend() {
	  return listFriend;
     }

     public void setListFriend(HashMap<Integer, String> listFriend) {
	  this.listFriend = listFriend;
     }

     public ArrayList<SendFriendRequest> getSendFriendRequestList() {
	  return sendFriendRequestList;
     }

     public void setSendFriendRequestList(ArrayList<SendFriendRequest> sendFriendRequestList) {
	  this.sendFriendRequestList = sendFriendRequestList;
     }

     public int getTempId() {
	  return tempId;
     }

     public void setTempId(int tempId) {
	  this.tempId = tempId;
     }

     public String getTempMessage() {
	  return tempMessage;
     }

     public void setTempMessage(String tempMessage) {
	  this.tempMessage = tempMessage;
     }

     public HashMap<Integer, String> getBlockList() {
	  return blockList;
     }

     public void setBlockList(HashMap<Integer, String> blockList) {
	  this.blockList = blockList;
     }
}
