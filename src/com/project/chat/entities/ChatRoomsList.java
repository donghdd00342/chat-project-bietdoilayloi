/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author DongPhuc
 */
public class ChatRoomsList implements Serializable {

    private ArrayList<Room> all;
    private ArrayList<Room> ofMine;

    public ChatRoomsList() {
        this.all = new ArrayList<>();
        this.ofMine = new ArrayList<>();
    }

    public ChatRoomsList(ArrayList<Room> all, ArrayList<Room> ofMine) {
        this.all = all;
        this.ofMine = ofMine;
    }

    public ArrayList<Room> getAll() {
        return all;
    }

    public void setAll(ArrayList<Room> all) {
        this.all = all;
    }

    public ArrayList<Room> getOfMine() {
        return ofMine;
    }

    public void setOfMine(ArrayList<Room> ofMine) {
        this.ofMine = ofMine;
    }
}
