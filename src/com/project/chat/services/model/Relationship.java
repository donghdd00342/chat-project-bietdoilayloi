/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.services.model;

import com.project.chat.services.Setting;

/**
 *
 * @author DongHo
 */
public class Relationship implements Table  {
     private int id;
     private int sub_id;
     private int friend_id;
     private int status;

     public Relationship(int sub_id, int friend_id, int status) {
	  this.sub_id = sub_id;
	  this.friend_id = friend_id;
	  this.status = status;
     }

     public Relationship(int id, int sub_id, int friend_id, int status) {
	  this.id = id;
	  this.sub_id = sub_id;
	  this.friend_id = friend_id;
	  this.status = status;
     }

     public Relationship(int id) {
	  this.id = id;
     }

     public Relationship() {
     }

     public int getId() {
	  return id;
     }

     public void setId(int id) {
	  this.id = id;
     }

     public int getSub_id() {
	  return sub_id;
     }

     public void setSub_id(int sub_id) {
	  this.sub_id = sub_id;
     }

     public int getFriend_id() {
	  return friend_id;
     }

     public void setFriend_id(int friend_id) {
	  this.friend_id = friend_id;
     }

     public int getStatus() {
	  return status;
     }

     public void setStatus(int status) {
	  this.status = status;
     }

     @Override
     public String getTable() {
	  return Setting.RELATIONSHIPS_TABLE_NAME;
     }
     
}
