/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.client.ui;

import com.project.chat.entities.LoginSuccessResponse;
import com.project.chat.entities.SendMe;
import com.project.chat.entities.Actions;
import com.project.chat.entities.AskFriendResponse;
import com.project.chat.entities.OfflineMessages;
import com.project.chat.entities.PrivateMessage;
import com.project.chat.entities.SendFriendRequest;
import com.project.chat.entities.User;
import com.project.chat.services.Untilities;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author DongHo
 */
public final class MainUserUI extends javax.swing.JFrame {

    LoginSuccessResponse loginSuccessResponse;
    Socket socket;
    User user;

    HashMap<Integer, String> friendList; // listFriend
    HashMap<Integer, OfflineMessages> allOfflineMsg; // offline messages
    ArrayList<SendFriendRequest> sendFriendRequestList;
    ArrayList<Integer> hasSentRequestList;
    HashMap<Integer, String> blockList; // block list

    // chosing chat room
    ChosingChatRoomUI chosingChatRoomUI;

    // CHAT
    HashMap<Integer, PrivateChatUI> privateChatList;
    HashMap<Integer, PublicChatRoomUI> publicChatRoomList;

    public UserInformationsUI userInformations;

    /**
     * Creates new form MainUserUI
     *
     * @param socket
     * @param loginSuccessResponse
     * @param privateChatList
     * @param publicChatRoomList
     */
    public MainUserUI(
            Socket socket,
            LoginSuccessResponse loginSuccessResponse,
            HashMap<Integer, PrivateChatUI> privateChatList,
            HashMap<Integer, PublicChatRoomUI> publicChatRoomList,
            ChosingChatRoomUI chosingChatRoomUI,
            ArrayList<Integer> hasSentRequestList
    ) {
        initComponents();
        // disable nút kết bạn
        btnKetBan.setEnabled(false);
        this.loginSuccessResponse = loginSuccessResponse;
        this.socket = socket;
        this.setLocationRelativeTo(null);

        this.friendList = loginSuccessResponse.getListFriend(); // danh sách bạn
        this.allOfflineMsg = loginSuccessResponse.getListOfflineMsg(); // tất cả tin nhắn chưa đọc của tất cả người gửi
        this.sendFriendRequestList = loginSuccessResponse.getSendFriendRequestList(); // danh sách lời mời kết bạn
        this.blockList = loginSuccessResponse.getBlockList(); // danh sách người dùng bị chặn
        this.user = loginSuccessResponse.getUser(); // thông tin user đang đăng nhập

        // Hiển thị tên user
        this.setTitle("Xin chào " + user.getFullname() + "!");
        // set danh sách bạn
        fillFriendList(this.friendList);
        // set danh sách tin nhắn chưa đọc
        fillAllOfflineMsg(this.allOfflineMsg);
        // set danh sách lời mời kết bạn chưa xử lý
        fillFriendRequestList(this.sendFriendRequestList);
        // set danh sách người dùng bị chặn
        fillBlockList(blockList);

        // khởi tạo danh sách đã gửi yêu cầu kết bạn
        this.hasSentRequestList = hasSentRequestList;
        // lấy danh sách chat riêng (private chat) từ ClientThread
        this.privateChatList = privateChatList;
        // lấy danh sách phòng chát chung (public chat)
        this.publicChatRoomList = publicChatRoomList;
        // tham chiếu chosingChatRoomUI
        this.chosingChatRoomUI = chosingChatRoomUI;
    }

    // CÁC HÀM VIẾT Ở ĐÂY setFriendList
    public void removeHasSentRequestList(int id) {
        if (this.hasSentRequestList.contains(id)) {
            this.hasSentRequestList.remove(Integer.valueOf(id));
            System.out.println("Đã remove...");
            // reset form
            txtKeyword.setText("");
        }
    }

    public void showAlert(String alertString) {
        if (alertString.length() != 0) {
            Untilities.alert(this, alertString);
        }
    }

    public void fillFriendRequestList(ArrayList<SendFriendRequest> sendFriendRequestList) {
        this.tabPane.setTitleAt(2, "Lời mời kết bạn [" + sendFriendRequestList.size() + "]");
        this.sendFriendRequestList = sendFriendRequestList;
        DefaultTableModel model = (DefaultTableModel) tableFriendRequest.getModel();
        model.setRowCount(0);
        sendFriendRequestList.forEach((sendFriendRequest) -> {
            model.addRow(new Object[]{sendFriendRequest.getFromFullName(), sendFriendRequest.getMessage(), sendFriendRequest});
        });
        System.out.println("[FILL] Lời mời kết bạn của " + this.user.getFullname());
    }

    public void fillFriendList(HashMap<Integer, String> listFriend) {
        this.tabPane.setTitleAt(0, "Danh sách bạn bè [" + listFriend.size() + "]");
        this.friendList = listFriend; // đồng bộ lại
        DefaultTableModel model = (DefaultTableModel) tableFriendList.getModel();
        model.setRowCount(0);
        listFriend.entrySet().forEach((entry) -> {
            Integer key = entry.getKey();
            String value = entry.getValue();
            model.addRow(new String[]{Integer.toString(key), " " + value});
        });
        System.out.println("[FILL] Danh sách bạn bè của " + this.user.getFullname());
    }
    
    public void fillAllOfflineMsg(HashMap<Integer, OfflineMessages> allMsgOffline) {
        this.tabPane.setTitleAt(1, "Tin nhắn chưa đọc [" + allMsgOffline.size() + "+]");
        this.allOfflineMsg = allMsgOffline; // đồng bộ lại
        DefaultTableModel model = (DefaultTableModel) tableOfflineMessages.getModel();
        model.setRowCount(0);
        allMsgOffline.entrySet().forEach((entry) -> {
            Integer key = entry.getKey();
            OfflineMessages value = entry.getValue();
            model.addRow(new String[]{Integer.toString(key), " " + value.getFromName(), " " + value.getLastMsg(), "" + value.getRelationshipId()});
        });
        System.out.println("[FILL] Tất cả tin nhắn chưa đọc theo từng người gửi");
    }

    public void fillBlockList(HashMap<Integer, String> blockList) {
        this.tabPane.setTitleAt(3, "Người dùng bị chặn [" + blockList.size() + "]");
        this.blockList = blockList;
        DefaultTableModel model = (DefaultTableModel) tableBlockFriend.getModel();
        model.setRowCount(0);
        blockList.entrySet().forEach((entry) -> {
            Integer key = entry.getKey();
            String value = entry.getValue();
            model.addRow(new String[]{Integer.toString(key), " " + value});
        });
        System.out.println("[FILL] Người dùng bị chặn của " + this.user.getFullname());
    }

    public void fillFriendsResultList(HashMap<Integer, String> listUserResult) {
        DefaultTableModel model = (DefaultTableModel) tableFriendList.getModel();
        model.setRowCount(0);
        // loại bỏ mình khỏi danh sách tìm kiếm
        listUserResult.remove(this.user.getId());
        // loại bỏ sự trùng lặp danh sách bạn
        for (Map.Entry<Integer, String> entry : this.friendList.entrySet()) {
            Integer key = entry.getKey();
            if (listUserResult.containsKey(key)) {
                listUserResult.remove(key);
            }
        }
        listUserResult.entrySet().forEach((entry) -> {
            Integer key = entry.getKey();
            String value = entry.getValue();
            model.addRow(new String[]{Integer.toString(key), " " + value});
        });
        System.out.println("[FILL] kết quả tìm kiếm vào danh sách bạn bè của " + this.user.getFullname());
        if (listUserResult.size() > 0) {
            btnKetBan.setEnabled(true);
        } else {
            btnKetBan.setEnabled(false);
            Untilities.alert(this, "Không tìm thấy kết quả nào.");
        }
    }

    private void timTrongDanhSach(String keyword) {
        DefaultTableModel model = (DefaultTableModel) tableFriendList.getModel();
        model.setRowCount(0);
        HashMap<Integer, String> myList = new HashMap<>();
        this.friendList.entrySet().forEach((entry) -> {
            Integer key = entry.getKey();
            String value = entry.getValue();
            if (value.toLowerCase().contains(keyword.toLowerCase())) {
                model.addRow(new String[]{Integer.toString(key), " " + value});
            }
        });
        if (0 == model.getRowCount()) {
            Untilities.alert(this, "Không tìm thấy kết quả nào!");
        }
    }

    private void timToanMangLuoi(String keyword) {
        Untilities.sendTo(new SendMe(Actions.SEARCH_FRIENDS, keyword), socket);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabPane = new javax.swing.JTabbedPane();
        panelFriendsList = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableFriendList = new javax.swing.JTable();
        txtKeyword = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        cbbSearchOption = new javax.swing.JComboBox<>();
        btnSearch = new javax.swing.JButton();
        btnKetBan = new javax.swing.JButton();
        btnXoaBan = new javax.swing.JButton();
        btnChatRieng = new javax.swing.JButton();
        btnSignOut = new javax.swing.JButton();
        btnChatRoom = new javax.swing.JButton();
        btnContacts = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        panelOfflineMessages = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableOfflineMessages = new javax.swing.JTable();
        jButton7 = new javax.swing.JButton();
        panelFriendRequest = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableFriendRequest = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        panelBlockList = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableBlockFriend = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        tableFriendList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Họ và Tên"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableFriendList.setToolTipText("Danh sách bạn bè");
        jScrollPane1.setViewportView(tableFriendList);
        if (tableFriendList.getColumnModel().getColumnCount() > 0) {
            tableFriendList.getColumnModel().getColumn(0).setMinWidth(0);
            tableFriendList.getColumnModel().getColumn(0).setPreferredWidth(0);
            tableFriendList.getColumnModel().getColumn(0).setMaxWidth(0);
            tableFriendList.getColumnModel().getColumn(1).setResizable(false);
        }

        txtKeyword.setToolTipText("Nhập vào tên để tìm kiếm");
        txtKeyword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtKeywordKeyPressed(evt);
            }
        });

        jLabel1.setText("Tìm kiếm");

        cbbSearchOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Trong danh sách", "Trên toàn mạng lưới" }));
        cbbSearchOption.setToolTipText("Tùy chọn tìm kiếm");
        cbbSearchOption.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbSearchOptionActionPerformed(evt);
            }
        });

        btnSearch.setToolTipText("Tìm kiếm");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnKetBan.setToolTipText("Thêm bạn");
        btnKetBan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKetBanActionPerformed(evt);
            }
        });

        btnXoaBan.setToolTipText("Xóa bạn");
        btnXoaBan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaBanActionPerformed(evt);
            }
        });

        btnChatRieng.setToolTipText("Chat riêng");
        btnChatRieng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChatRiengActionPerformed(evt);
            }
        });

        btnSignOut.setToolTipText("Đăng xuất");
        btnSignOut.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnSignOut.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnSignOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSignOutActionPerformed(evt);
            }
        });

        btnChatRoom.setToolTipText("Vào phòng chat");
        btnChatRoom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChatRoomActionPerformed(evt);
            }
        });

        btnContacts.setToolTipText("Tải lại danh sách bạn bè");
        btnContacts.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnContacts.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnContacts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnContactsActionPerformed(evt);
            }
        });

        jButton4.setToolTipText("Thông tin cá nhân");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelFriendsListLayout = new javax.swing.GroupLayout(panelFriendsList);
        panelFriendsList.setLayout(panelFriendsListLayout);
        panelFriendsListLayout.setHorizontalGroup(
            panelFriendsListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFriendsListLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelFriendsListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelFriendsListLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtKeyword, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbbSearchOption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelFriendsListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnContacts, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnChatRoom, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnChatRieng, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnXoaBan, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnKetBan, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSignOut, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelFriendsListLayout.setVerticalGroup(
            panelFriendsListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFriendsListLayout.createSequentialGroup()
                .addGroup(panelFriendsListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelFriendsListLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(panelFriendsListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtKeyword, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)))
                    .addGroup(panelFriendsListLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(cbbSearchOption))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFriendsListLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnSearch)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelFriendsListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelFriendsListLayout.createSequentialGroup()
                        .addComponent(btnKetBan, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnXoaBan, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnChatRieng, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnChatRoom, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnContacts, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSignOut, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 533, Short.MAX_VALUE))
                .addContainerGap())
        );

        tabPane.addTab("Danh sách bạn bè", panelFriendsList);

        tableOfflineMessages.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "Gửi từ", "Một phần của nội dung", "relationshipId"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableOfflineMessages.setToolTipText("Tin nhắn offline của bạn");
        jScrollPane2.setViewportView(tableOfflineMessages);
        if (tableOfflineMessages.getColumnModel().getColumnCount() > 0) {
            tableOfflineMessages.getColumnModel().getColumn(0).setMinWidth(0);
            tableOfflineMessages.getColumnModel().getColumn(0).setPreferredWidth(0);
            tableOfflineMessages.getColumnModel().getColumn(0).setMaxWidth(0);
            tableOfflineMessages.getColumnModel().getColumn(1).setPreferredWidth(15);
            tableOfflineMessages.getColumnModel().getColumn(3).setMinWidth(0);
            tableOfflineMessages.getColumnModel().getColumn(3).setPreferredWidth(0);
            tableOfflineMessages.getColumnModel().getColumn(3).setMaxWidth(0);
        }

        jButton7.setToolTipText("Đọc tin nhắn");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelOfflineMessagesLayout = new javax.swing.GroupLayout(panelOfflineMessages);
        panelOfflineMessages.setLayout(panelOfflineMessagesLayout);
        panelOfflineMessagesLayout.setHorizontalGroup(
            panelOfflineMessagesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOfflineMessagesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton7)
                .addContainerGap(29, Short.MAX_VALUE))
        );
        panelOfflineMessagesLayout.setVerticalGroup(
            panelOfflineMessagesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOfflineMessagesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelOfflineMessagesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelOfflineMessagesLayout.createSequentialGroup()
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 571, Short.MAX_VALUE))
                .addContainerGap())
        );

        tabPane.addTab("Tin nhắn chưa đọc", panelOfflineMessages);

        tableFriendRequest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Yêu cầu kết bạn từ", "Tin nhắn", "SendFriendRequest"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableFriendRequest.setToolTipText("Lời mời kết bạn chưa xử lý");
        jScrollPane3.setViewportView(tableFriendRequest);
        if (tableFriendRequest.getColumnModel().getColumnCount() > 0) {
            tableFriendRequest.getColumnModel().getColumn(0).setPreferredWidth(15);
            tableFriendRequest.getColumnModel().getColumn(2).setMinWidth(0);
            tableFriendRequest.getColumnModel().getColumn(2).setPreferredWidth(0);
            tableFriendRequest.getColumnModel().getColumn(2).setMaxWidth(0);
        }

        jButton2.setToolTipText("Đồng ý");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton10.setToolTipText("Từ chối");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton1.setToolTipText("Chặn người dùng này");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelFriendRequestLayout = new javax.swing.GroupLayout(panelFriendRequest);
        panelFriendRequest.setLayout(panelFriendRequestLayout);
        panelFriendRequestLayout.setHorizontalGroup(
            panelFriendRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFriendRequestLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelFriendRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelFriendRequestLayout.createSequentialGroup()
                        .addGroup(panelFriendRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton2)
                            .addComponent(jButton1))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelFriendRequestLayout.setVerticalGroup(
            panelFriendRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFriendRequestLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelFriendRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 571, Short.MAX_VALUE)
                    .addGroup(panelFriendRequestLayout.createSequentialGroup()
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        tabPane.addTab("Lời mời kết bạn", panelFriendRequest);

        tableBlockFriend.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Họ và Tên"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tableBlockFriend);
        if (tableBlockFriend.getColumnModel().getColumnCount() > 0) {
            tableBlockFriend.getColumnModel().getColumn(0).setMinWidth(0);
            tableBlockFriend.getColumnModel().getColumn(0).setPreferredWidth(0);
            tableBlockFriend.getColumnModel().getColumn(0).setMaxWidth(0);
            tableBlockFriend.getColumnModel().getColumn(1).setResizable(false);
        }

        jButton3.setToolTipText("Bỏ chặn người dùng");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBlockListLayout = new javax.swing.GroupLayout(panelBlockList);
        panelBlockList.setLayout(panelBlockListLayout);
        panelBlockListLayout.setHorizontalGroup(
            panelBlockListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBlockListLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 411, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(11, Short.MAX_VALUE))
        );
        panelBlockListLayout.setVerticalGroup(
            panelBlockListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBlockListLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBlockListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 571, Short.MAX_VALUE)
                    .addGroup(panelBlockListLayout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        tabPane.addTab("Người dùng bị chặn", panelBlockList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabPane)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

     private void btnContactsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnContactsActionPerformed
         // TODO add your handling code here:
         reloadFriendList();
     }//GEN-LAST:event_btnContactsActionPerformed

     private void btnSignOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSignOutActionPerformed
         // TODO add your handling code here:
         if (0 == JOptionPane.showConfirmDialog(this, "Bạn có chắc là muốn đăng xuất?")) {
             // thông báo đến các bạn đang chát
             this.privateChatList.forEach((k, v) -> {
                 // gửi PrivateMessage tới server
                 Untilities.sendTo(
                         new SendMe(
                                 Actions.SEND_PRIVATE_MESSAGE,
                                 new PrivateMessage(
                                         user.getId(),
                                         "SYSTEM",
                                         k,
                                         this.friendList.get(k),
                                         user.getFullname() + " đã đăng xuất! tuy nhiên bạn vẫn có thể gửi tin nhắn offline."
                                 )),
                         socket
                 );

             });
             // dispose all
             if (this.chosingChatRoomUI != null) {
                 this.chosingChatRoomUI.dispose();
             }
             // CHAT
             this.privateChatList.forEach((t, u) -> {
                 if (u != null) {
                     u.dispose();
                 }
             });
             this.privateChatList.clear();
             this.publicChatRoomList.forEach((t, u) -> {
                 if (u != null) {
                     u.dispose();
                 }
             });
             this.publicChatRoomList.clear();
             // gửi thông báo Logout lên server
             Untilities.sendTo(new SendMe(Actions.CLIENT_LOGOUT, this.user), this.socket);
             this.dispose();
             LoginUI.getInstance().setVisible(true);
         }
     }//GEN-LAST:event_btnSignOutActionPerformed

     private void btnXoaBanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaBanActionPerformed
         // Hành động nút xóa bạn
         int row = tableFriendList.getSelectedRow();
         if (row != -1) {
             if (JOptionPane.showConfirmDialog(this, "Bạn có chắc là muốn xóa kết bạn?") == 0) {
                 // Gửi thông tin xóa bạn lên server
                 TableModel tblModel = tableFriendList.getModel();
                 int friendId = (int) Integer.valueOf(tblModel.getValueAt(row, 0).toString());
                 String name = tblModel.getValueAt(row, 1).toString();
                 Untilities.sendTo(new SendMe(Actions.USER_DELETE_FRIEND, new SendFriendRequest(friendId, this.user.getId(), null, name)), this.socket);
             }
         } else {
             Untilities.alert(this, "Vui lòng chọn để xóa!");
         }
     }//GEN-LAST:event_btnXoaBanActionPerformed

     private void btnKetBanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKetBanActionPerformed
         // Hành động nút kết bạn
         int row = tableFriendList.getSelectedRow();
         if (row != -1) {
             // xử lý gửi yêu cầu kết bạn
             TableModel tblModel = tableFriendList.getModel();
             int id = Integer.valueOf(tblModel.getValueAt(row, 0).toString());
             String name = tblModel.getValueAt(row, 1).toString();
             if (user.getId() == id) {
                 Untilities.alert(this, "Bạn không thể kết bạn với chính mình...");
             } else {
                 // mở cửa sổ yêu cầu
                 RequestFriendUI requestFriend = new RequestFriendUI(this.user.getId(), id, name, this.hasSentRequestList, this.socket, this.user.getFullname());
                 requestFriend.setVisible(true);
             }
         } else {
             Untilities.alert(this, "Vui lòng chọn bạn để gửi yêu cầu!");
         }
     }//GEN-LAST:event_btnKetBanActionPerformed

     private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
         // ACTION NHẤN NÚT TÌM KIẾM
         searchKeyPress();
     }//GEN-LAST:event_btnSearchActionPerformed

     private void cbbSearchOptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbSearchOptionActionPerformed
         // TODO add your handling code here:
     }//GEN-LAST:event_cbbSearchOptionActionPerformed

     private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
         // Hành động đồng ý
         int row = tableFriendRequest.getSelectedRow();
         if (row != -1) {
             TableModel tblModel = tableFriendRequest.getModel();
             SendFriendRequest sendFriendRequest = (SendFriendRequest) tblModel.getValueAt(row, 2);
             Untilities.sendTo(new SendMe(Actions.ASK_FRIEND_RESPONSE, new AskFriendResponse(1, sendFriendRequest)), socket);
         } else {
             Untilities.alert(this, "Vui lòng chọn để xử lý!");
         }
     }//GEN-LAST:event_jButton2ActionPerformed

     private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
         // Hành động từ chối
         int row = tableFriendRequest.getSelectedRow();
         if (row != -1) {
             TableModel tblModel = tableFriendRequest.getModel();
             SendFriendRequest sendFriendRequest = (SendFriendRequest) tblModel.getValueAt(row, 2);
             Untilities.sendTo(new SendMe(Actions.ASK_FRIEND_RESPONSE, new AskFriendResponse(2, sendFriendRequest)), socket);
         } else {
             Untilities.alert(this, "Vui lòng chọn để xử lý!");
         }
     }//GEN-LAST:event_jButton10ActionPerformed

     private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
         // Hành động chặn
         int row = tableFriendRequest.getSelectedRow();
         if (row != -1) {
             TableModel tblModel = tableFriendRequest.getModel();
             SendFriendRequest sendFriendRequest = (SendFriendRequest) tblModel.getValueAt(row, 2);
             Untilities.sendTo(new SendMe(Actions.ASK_FRIEND_RESPONSE, new AskFriendResponse(3, sendFriendRequest)), socket);
         } else {
             Untilities.alert(this, "Vui lòng chọn để xử lý!");
         }
     }//GEN-LAST:event_jButton1ActionPerformed

     private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
         // Hành động nút bỏ chặn
         int row = tableBlockFriend.getSelectedRow();
         if (row != -1) {
             TableModel tblModel = tableBlockFriend.getModel();
             int friendId = (int) Integer.valueOf(tblModel.getValueAt(row, 0).toString());
             String name = tblModel.getValueAt(row, 1).toString();
             if (JOptionPane.showConfirmDialog(this, "Bạn có chắc là muốn bỏ chặn " + name + "?") == 0) {
                 // Gửi thông tin xóa bạn lên server
                 Untilities.sendTo(new SendMe(Actions.USER_SET_UNBLOCK, new SendFriendRequest(friendId, this.user.getId(), null, name)), this.socket);
             }
         } else {
             Untilities.alert(this, "Vui lòng chọn để bỏ chặn!");
         }
     }//GEN-LAST:event_jButton3ActionPerformed

     private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
         // Hành động xem thông tin cá nhân
         // mở cửa sổ thông tin cá nhân
         this.userInformations = new UserInformationsUI(this.socket, this.user);
         this.userInformations.setVisible(true);
     }//GEN-LAST:event_jButton4ActionPerformed

     private void txtKeywordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtKeywordKeyPressed
         // TODO add your handling code here:
         // Nhấn Enter
         if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
             searchKeyPress();
         }
     }//GEN-LAST:event_txtKeywordKeyPressed

     private void btnChatRiengActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChatRiengActionPerformed
         // Hành động nút chat riêng
         int row = tableFriendList.getSelectedRow();
         if (row != -1) {
             TableModel tblModel = tableFriendList.getModel();
             int id = Integer.valueOf(tblModel.getValueAt(row, 0).toString());
             String name = tblModel.getValueAt(row, 1).toString();
             // kiểm tra xem id thằng muốn chát có trong danh sách đang chát ko? có thì kiểm tra tiếp xem nó có đang ẩn ko? có thì hiện của sổ đó lên... focus
             if (privateChatList.containsKey(id)) {
                 if (privateChatList.get(id).isVisible()) {
                     // focus
                     privateChatList.get(id).setState(Frame.NORMAL);
                 } else {
                     // show
                     privateChatList.get(id).setVisible(true);
                 }
             } else {
                 // tạo cửa sổ chat riêng với id đó
                 PrivateChatUI temp_ = new PrivateChatUI(id, name, socket, user);
                 privateChatList.put(id, temp_);
                 temp_.setVisible(true);
             }
         } else {
             Untilities.alert(this, "Vui lòng chọn bạn để chat riêng!");
         }
     }//GEN-LAST:event_btnChatRiengActionPerformed

     private void btnChatRoomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChatRoomActionPerformed
         // Hành động vào phòng chát chung
         // kiểm tra IF - nếu ChosingChatRoomUI != null -> set visiable true - > focus
         if (this.chosingChatRoomUI != null) {
             this.chosingChatRoomUI.setVisible(true);
             this.chosingChatRoomUI.setState(Frame.NORMAL);
         } else {
             this.chosingChatRoomUI = ChosingChatRoomUI.getInstance(this.socket, this.user, this.publicChatRoomList, this.hasSentRequestList);
             this.chosingChatRoomUI.setVisible(true);
         }
     }//GEN-LAST:event_btnChatRoomActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // Hành động đọc tin nhắn offline
        int row = tableOfflineMessages.getSelectedRow();
         if (row != -1) {
             TableModel tblModel = tableOfflineMessages.getModel();
             int id = Integer.valueOf(tblModel.getValueAt(row, 0).toString());
             String name = tblModel.getValueAt(row, 1).toString();
             int relationshipId = Integer.valueOf(tblModel.getValueAt(row, 3).toString());
             // kiểm tra xem id thằng gửi có trong danh sách đang chát ko?
             // ---> có thì kiểm tra tiếp xem nó có đang ẩn ko? có thì hiện của sổ đó lên -> readOfflineMessage()
             // ko thì tạo cửa sổ chát và readOfflineMessage()
             if (privateChatList.containsKey(Integer.valueOf(id))) {
                 if (privateChatList.get(Integer.valueOf(id)).isVisible()) {
                     // focus
                     privateChatList.get(Integer.valueOf(id)).setState(Frame.NORMAL);
                 } else {
                     // show
                     privateChatList.get(Integer.valueOf(id)).setVisible(true);
                 }
                 // readOfflineMessage()
                 readOfflineMessage(privateChatList.get(Integer.valueOf(id)), this.allOfflineMsg.get(Integer.valueOf(id)).getMsgList(), this.allOfflineMsg.get(Integer.valueOf(id)));
             } else {
                 // tạo cửa sổ chat riêng với id đó
                 PrivateChatUI temp_ = new PrivateChatUI(id, name, socket, user);
                 privateChatList.put(Integer.valueOf(id), temp_);
                 temp_.setVisible(true);
                 // readOfflineMessage()
                 readOfflineMessage(temp_, this.allOfflineMsg.get(Integer.valueOf(id)).getMsgList(), this.allOfflineMsg.get(Integer.valueOf(id)));
             }
         } else {
             Untilities.alert(this, "Vui lòng chọn để đọc!");
         }
    }//GEN-LAST:event_jButton7ActionPerformed

    // functions
    private void readOfflineMessage(PrivateChatUI privateChatUI, ArrayList<String> listMsg, OfflineMessages offlineMsg) {
        // set nội dung cũ = ""
        privateChatUI.txtAreaCurrentChat.setText("");
        // append nội dung offline msg
        privateChatUI.txtAreaCurrentChat.append("------- Tin nhắn chưa đọc -------\n");
        listMsg.forEach((t) -> {
            privateChatUI.txtAreaCurrentChat.append(t);
        });
        // gửi thông báo lên server là đã đọc -> chuyển status = 1
        Untilities.sendTo(new SendMe(Actions.READ_OFFLINE_MSG, offlineMsg), socket);
    }
    
    public JButton getBtnKetBan() {
        return btnKetBan;
    }

    public JButton getBtnXoaBan() {
        return btnXoaBan;
    }

    private void searchKeyPress() {
        if (txtKeyword.getText().trim().equals("")) {
            Untilities.alert(this, "Hãy nhập từ khóa để tìm!");
        } else {
            switch (cbbSearchOption.getSelectedIndex()) {
                case 0:
                    timTrongDanhSach(txtKeyword.getText().trim());
                    btnKetBan.setEnabled(false);
                    btnXoaBan.setEnabled(true);
                    btnChatRieng.setEnabled(true);
                    break;
                case 1:
                    timToanMangLuoi(txtKeyword.getText().trim());
                    btnKetBan.setEnabled(true);
                    btnXoaBan.setEnabled(false);
                    btnChatRieng.setEnabled(false);
                    break;
            }
        }
    }

    public void reloadFriendList() {
        fillFriendList(this.friendList);
        txtKeyword.setText("");
        btnKetBan.setEnabled(false);
        btnXoaBan.setEnabled(true);
        btnChatRieng.setEnabled(true);
        tabPane.setSelectedIndex(0);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChatRieng;
    private javax.swing.JButton btnChatRoom;
    private javax.swing.JButton btnContacts;
    private javax.swing.JButton btnKetBan;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnSignOut;
    private javax.swing.JButton btnXoaBan;
    private javax.swing.JComboBox<String> cbbSearchOption;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPanel panelBlockList;
    private javax.swing.JPanel panelFriendRequest;
    private javax.swing.JPanel panelFriendsList;
    private javax.swing.JPanel panelOfflineMessages;
    private javax.swing.JTabbedPane tabPane;
    private javax.swing.JTable tableBlockFriend;
    private javax.swing.JTable tableFriendList;
    private javax.swing.JTable tableFriendRequest;
    private javax.swing.JTable tableOfflineMessages;
    private javax.swing.JTextField txtKeyword;
    // End of variables declaration//GEN-END:variables
}
