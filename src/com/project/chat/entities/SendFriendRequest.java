/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;

/**
 *
 * @author DongHo
 */
public class SendFriendRequest implements Serializable {

     private int fromId;
     private String fromFullName;
     private int toId;
     String message;

     public SendFriendRequest(int fromId, int toId, String message, String fromFullName) {
	  this.fromId = fromId;
	  this.fromFullName = fromFullName;
	  this.toId = toId;
	  this.message = message;
     }

     public String getFromFullName() {
	  return fromFullName;
     }

     public void setFromFullName(String fromFullName) {
	  this.fromFullName = fromFullName;
     }

     public int getFromId() {
	  return fromId;
     }

     public void setFromId(int fromId) {
	  this.fromId = fromId;
     }

     public int getToId() {
	  return toId;
     }

     public void setToId(int toId) {
	  this.toId = toId;
     }

     public String getMessage() {
	  return message;
     }

     public void setMessage(String message) {
	  this.message = message;
     }
}
