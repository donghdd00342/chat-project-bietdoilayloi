/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.services;

import com.project.chat.entities.Actions;
import com.project.chat.entities.ChatRoomMessage;
import com.project.chat.entities.Client;
import com.project.chat.entities.SendMe;
import com.project.chat.entities.User;
import java.awt.Component;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.JOptionPane;

import java.util.Properties;
import javax.crypto.BadPaddingException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author DongHo
 */
public class Untilities {

    private Untilities() {
    }

    // getUserFromMemberList: lấy User từ danh sách thành viên (phòng chat)
    public static User getUserFromMemberList(int id, ArrayList<User> userMemberList) {
        for (User user : userMemberList) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    // convert ListClient -> to ListUser
    public static ArrayList<User> convertListClient(ArrayList<Client> arrayListClient) {
        ArrayList<User> arrayListUser = new ArrayList<>();
        if (arrayListClient != null) {
            arrayListClient.forEach((c) -> {
                arrayListUser.add(c.getUser());
            });
        } else {
            return null;
        }
        return arrayListUser;
    }

    // SHARE...
    public static void sendTo(Object o, Socket socket) {
        try {
            ObjectOutputStream os = new ObjectOutputStream(socket.getOutputStream());
            os.flush();
            os.writeObject(o);
            os.flush();
        } catch (IOException e) {
            System.err.println("Lỗi:  " + e);
        }
    }

    public static void sendTo(Object o, ArrayList<Client> arrayListClient) {
        arrayListClient.forEach((t) -> {
                Untilities.sendTo(o, t.getSocket());
        });
    }

    public static String[] toArray(Set set) {
        String[] myString = new String[set.size()];
        Iterator itr = set.iterator();
        int i = 0;
        while (itr.hasNext()) {
            myString[i] = itr.next().toString();
            ++i;
        }
        return myString;
    }
    // End - SHARE

    // CLIENT ...    
    public static void alert(Component c, String message) {
        JOptionPane.showMessageDialog(c, message);
    }

    public static void alert(String message) {
        JOptionPane.showMessageDialog(null, message);
    }
    // End - CLIENT

    // SERVER...
    public static Client getClientFromId(int id, Map<Integer, Client> clientMap) {
        if (clientMap.containsKey(Integer.valueOf(id))) {
            System.out.println("[CHECK] id = " + id + " (" + clientMap.get(Integer.valueOf(id)).getUser().getFullname() + " có online)");
            return clientMap.get(Integer.valueOf(id));
        }
        return null;
    }

    public static boolean isUserOnline(int toId, Map<Integer, Client> clientMap) {
        return clientMap.containsKey(toId);
    }

    // gửi mail
    public static boolean sendEmailTo(String fullName, String toEmail, String pass) {
        System.out.println("Bắt đầu chương trình gửi mail.....................");
        Properties props = new Properties();

        props.put("mail.smtp.user", Setting.SYS_EMAIL);
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "25");
        props.put("mail.debug", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.EnableSSL.enable", "true");

        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Setting.SYS_EMAIL, Setting.SYS_PASSWORD);
            }
        });
        try {
            StringBuilder bodyMailStr = new StringBuilder();
            bodyMailStr.append("Xin chào ").append(fullName).append(",");
            bodyMailStr.append("\n\nMật khẩu đăng nhập của bạn là: ").append(Untilities.decrypt(pass));
            bodyMailStr.append("\n\nHava fun :)");

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(Setting.SYS_EMAIL));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            message.setSubject("[Quên mật khẩu] Bạn đã yêu cầu gửi mật khẩu qua email.");
            message.setText(bodyMailStr.toString());
            Transport.send(message);
            System.out.println("Done! Đã gửi mật khẩu qua email..............");
            return true;
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    // security
    public static String encrypt(String password) {
        try {
            IvParameterSpec ivp = new IvParameterSpec(Setting.SECURITY_INIT_VECTOR.getBytes());
            SecretKeySpec skey = new SecretKeySpec(Setting.SECURITY_KEY.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skey, ivp);
            byte[] encryptByte = cipher.doFinal(password.getBytes());
            return Base64.encodeBase64String(encryptByte);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            System.err.println("Lỗi mã hóa: " + e);
            return "";
        }

    }

    public static String decrypt(String encrypted) {
        try {
            IvParameterSpec ivp = new IvParameterSpec(Setting.SECURITY_INIT_VECTOR.getBytes());
            SecretKeySpec skey = new SecretKeySpec(Setting.SECURITY_KEY.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skey, ivp);
            byte[] decryptByte = cipher.doFinal(Base64.decodeBase64(encrypted));
            return new String(decryptByte);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            System.err.println("Lỗi giải mã: " + e);
            return "";
        }
    }
}
