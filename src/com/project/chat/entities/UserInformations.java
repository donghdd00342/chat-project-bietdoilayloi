/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.chat.entities;

import java.io.Serializable;

/**
 *
 * @author DongHo
 */
public class UserInformations implements Serializable {

     int id;
     String email;
     String fullName;
     String currentPass;
     String newPass;

     public UserInformations(int id, String fullName, String currentPass, String newPass) {
	  this.id = id;
	  this.fullName = fullName;
	  this.currentPass = currentPass;
	  this.newPass = newPass;
     }

     public UserInformations(int id, String email, String fullName, String currentPass, String newPass) {
	  this.id = id;
	  this.email = email;
	  this.fullName = fullName;
	  this.currentPass = currentPass;
	  this.newPass = newPass;
     }

     public int getId() {
	  return id;
     }

     public void setId(int id) {
	  this.id = id;
     }

     public String getFullName() {
	  return fullName;
     }

     public void setFullName(String fullName) {
	  this.fullName = fullName;
     }

     public String getCurrentPass() {
	  return currentPass;
     }

     public void setCurrentPass(String currentPass) {
	  this.currentPass = currentPass;
     }

     public String getNewPass() {
	  return newPass;
     }

     public void setNewPass(String newPass) {
	  this.newPass = newPass;
     }

     public String getEmail() {
	  return email;
     }

     public void setEmail(String email) {
	  this.email = email;
     }
}
